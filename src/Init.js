import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { /* fetchTokens, */ fetchLoadData } from 'modules/User';
import { actions } from 'modules/Socket';

/**
 * Init.
 * @param {object} values values.
 */
class Init extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    const { authenticated, loadData, init, sconnect } = props;
    sconnect();
    if (authenticated && !init) {
      loadData();
    }
  }

  /**
   * Render
   * @returns {object}.
   */
  render() {
    return <div />;
  }
}

Init.propTypes = {
  init: PropTypes.bool.isRequired,
  loadData: PropTypes.func.isRequired,
  sconnect: PropTypes.func.isRequired,
  authenticated: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  init: state.user.init,
  authenticated: state.user.token !== '',
});

const mapDispatchToProps = dispatch => ({
  loadData() {
    dispatch(fetchLoadData());
  },
  sconnect() {
    dispatch(actions.socketConnect());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Init);
