module.exports = {
  // start Nav
  'nav.burger': 'Меню',
  'nav.ru': 'Русский',
  'nav.en': 'English',
  'nav.signin': 'Вход',
  'nav.signup': 'Регистрация',
  'nav.forgot_password': 'Забыли пароль?',
  // end Nav

  // start Sidebar
  'sidebar.settings': 'Настройки',
  'sidebar.change_password': 'Сменить пароль',
  'sidebar.logout': 'Выйти',
  // end Sidebar

  // start sign up
  'signup.title': 'Регистрация',
  'signup.subtitle': 'Ввести данные',
  'signup.btn_title': 'Зарегистрироваться',
  INVALID_TOKEN: 'Неверный код.',
  VALIDATION_EMAIL_ALREADY_EXIST: 'Данный email адрес уже существует.',
  'signup.success_message':
    'На указанную почту отправлено письмо со ссылкой для подтверждения регистрации. Если оно не приходит, вернитесь на страницу регистрации, чтобы ввести данные заново, или обратитесь в техническую поддержку.',
  // end sign up

  // start verify
  'verify.success_message': 'Вы успешно зарегистрировались. Чтобы войти, ',
  'verify.success_message_link': 'введите логин и пароль',
  // end verify

  // start resend
  'resend.success_message': 'Письмо отправлено',
  // end resend

  // start sign in
  'signin.title': 'Вход',
  'signin.subtitle': 'Введите свои данные',
  'signin.password_old': 'Старый пароль',
  'signin.password_new': 'Новый пароль',
  'signin.password_confirm': 'Подтвердите новый пароль',
  'signin.btn_title': 'Войти',
  'signin.repeat':
    'Регистрация не подтверждена. Следуйте инструкции в письме. {repeat} отправку подтверждающего письма',
  // end sign in

  // start logout
  'logout.title': 'Выход из учётной записи',
  'logout.success_message': 'Сеанс завершён',
  // end logout

  // start forgot-password
  'forgotPassword.title': 'Забыли пароль?',
  'forgotPassword.subtitle': 'Создайте новый, чтобы войти в учётную запись',
  'forgotPassword.btn_title': 'Отправить',
  'forgotPassword.success_message':
    'На указанную почту отправлено письмо со ссылкой для сброса пароля. Если оно не приходит, вернитесь на страницу сброса пароля и попробуйте заново, или обратитесь в техническую поддержку.',
  // end forgot-password

  // start reset-password
  'resetPassword.title': 'Форма сброса пароля',
  'resetPassword.btn_title': 'Сбросить',
  'resetPassword.success_message': 'Пароль сброшен.',
  // end reset-password

  // start page ChangePassword
  'changePassword.title': 'Сменить пароль',
  'changePassword.btn_title': 'Сменить',
  'changePassword.success_message':
    'Вы успешно изменили пароль. Запомните или запишите его',
  // end page ChangePassword

  // start page Home
  'home.title': 'ЗДЕСЬ БУДЕТ ПРИВЕТСВЕННЫЙ ТЕКСТ И КАРТИНКИ',
  // end page Home

  // start page NotFound
  'notFound.title': 'Ошибка 404.',
  'notFound.subtitle': 'Страница не существует',
  // end page NotFound

  // start page Forms
  'form.email': 'почта',
  'form.password': 'пароль',
  'form.password_old': 'старый пароль',
  'form.password_new': 'новый пароль',
  'form.confirm_password': 'подтвердить',
  'form.agree': 'Я согласен с ....',
  'form.code': 'Код',
  'form.error_required': 'Обязательно',
  'form.error_invalid_email': 'Неправильный адрес электронной почты',
  'form.error_invalid_phone': 'Неправильный телефон (требуется 11 цифр)',
  'form.error_password_min': 'Не менее {min} знаков',
  'form.error_password_letters': 'Должен содержать буквы',
  'form.error_password_num': 'Должен содержать цифры',
  'form.error_password_match': 'Пароли не совпадают',
  // end page Forms

  // common word
  'common.repeat': 'Повторить',
  'common.success_message': 'Успешно',
  // end cpmmom word

  // Validate
  MUST_BE_CONTAINS_LETTERS: 'Пароль должен включать минимум 1 латинскую букву',
  MUST_BE_CONTAINS_NUMBER: 'Пароль должен включать числа',
  MUST_BE_7_CHARACTERS_OR_MORE: 'Пароль должен содержать 7 или более символов',
  MUST_BE_4_CHARACTERS_OR_MORE: 'Пароль должен содержать 4 или более символов',
  REQUIRED: 'Поле обязательно для заполнения',
  PASSWORD_DO_NOT_MATCH: 'Пароли не совпадают',
  INVALID_EMAIL_ADDRESS: 'Некорректный email адрес',
  // end Validate

  // Errors
  INVALID_LOGIN_DATA: 'Некорректный email или пароль.',
  CANNOT_FOUND_USER_WITH_THIS_EMAIL: 'Пользователь с таким email не найден.',
  INVALID_OLD_PASSWORD: 'Неверно указан старый пароль',
  EMPTY_OR_VERY_SHORT_PASSWORD: 'Не указан или слишком короткий пароль',
  // End errors

  // Stub
  simulators: 'Тренажеры',
  stub1: 'Справочники',
  reports: 'Отчеты',
  // End Stub

  // Главный фильтр (Правило)
  RULE_SIMPLE_SUM: 'Без формул',
  RULE_FIVE_SUM: 'Формулы на 5',
  RULE_TEN_SUM: 'Формула на 10',
  RULE_FIVE_AND_TEN_SUM: 'Комби формулы',
  NO_MODE: 'Произвольные формулы',
  // Главный фильтр

  // Главный фильтр (Раздел)
  SECTION_UP_TO_FOUR: 'Счет 1,2,3,4',
  SECTION_UP_TO_FIVE: 'Счет до 5',
  SECTION_UP_TO_SIX: 'Счет до 6',
  SECTION_UP_TO_SEVEN: 'Счет до 7',
  SECTION_UP_TO_EIGHT: 'Счет до 8',
  SECTION_UP_TO_NINE: 'Счет до 9',
  SECTION_MIRROR: 'Зеркальные',
  SECTION_TEN: 'Десятки',
  // Главный фильтр

  // Меню
  DIRECTORY: 'Справочники',

  // Student
  // Table
  STUDENT_TITLE: 'Ученики',
  STUDENT_TITLE_SINGULAR: 'Ученика',
  STUDENT_FORM_LEFT_PATH_TITLE: 'Данные ученика',
  STUDENT_FORM_RIGTH_PATH_TITLE: 'Данные родителя',
  STUDENT_TABLE_CREATE_TITLE: 'Добавить нового ученика',
  STUDENT_TABLE_SAVE_TITLE: 'Редактировать данные ученика',
  STUDENT_TABLE_CREATE: 'Добавить',
  STUDENT_TABLE_SAVE: 'Сохранить',
  STUDENT_SUCCESS_CREATE_MESSAGE: 'Ученик добавлен',
  STUDENT_SUCCESS_SAVE_MESSAGE: 'Ученик успешно изменен',

  // Group
  // Table
  GROUP_TITLE: 'Группы',
  GROUP_TITLE_SINGULAR: 'Группа',
  GROUP_FORM_LEFT_PATH_TITLE: 'Основные поля',
  GROUP_FORM_RIGTH_PATH_TITLE: 'Дополнительные поля',
  GROUP_TABLE_CREATE_TITLE: 'Добавить новую группу',
  GROUP_TABLE_SAVE_TITLE: 'Редактировать данные группы',
  GROUP_TABLE_CREATE: 'Добавить',
  GROUP_TABLE_SAVE: 'Сохранить',
  GROUP_SUCCESS_CREATE_MESSAGE: 'Группа добавлена',
  GROUP_SUCCESS_SAVE_MESSAGE: 'Группа успешно изменена',

  // School
  // Table
  SCHOOL_TITLE: 'Школы',
  SCHOOL_FORM_LEFT_PATH_TITLE: 'Данные руководителя школы',
  SCHOOL_FORM_RIGTH_PATH_TITLE: 'Данные школы',
  SCHOOL_TABLE_CREATE_TITLE: 'Добавить новую школу',
  SCHOOL_TABLE_SAVE_TITLE: 'Редактировать данные школы',
  SCHOOL_TABLE_CREATE: 'Добавить',
  SCHOOL_TABLE_SAVE: 'Сохранить',
  SCHOOL_SUCCESS_CREATE_MESSAGE: 'Школа добавлена',
  SCHOOL_SUCCESS_SAVE_MESSAGE: 'Школа успешно изменена',

  // Teacher
  // Table
  TEACHER_TITLE: 'Педагоги',
  TEACHER_FORM_LEFT_PATH_TITLE: 'Основные поля',
  TEACHER_FORM_RIGTH_PATH_TITLE: 'Дополнительные поля',
  TEACHER_TABLE_CREATE_TITLE: 'Добавить нового педагога',
  TEACHER_TABLE_SAVE_TITLE: 'Редактировать данные педагога',
  TEACHER_TABLE_CREATE: 'Добавить',
  TEACHER_TABLE_SAVE: 'Сохранить',
  TEACHER_SUCCESS_CREATE_MESSAGE: 'Педагог добавлен',
  TEACHER_SUCCESS_SAVE_MESSAGE: 'Педагог успешно изменен',

  // Homework
  // Table
  HOMEWORK_TITLE: 'Домашнее задание',
  HOMEWORK_TITLE_SINGULAR: 'Домашнее задание',
  HOMEWORK_FORM_LEFT_PATH_TITLE: 'Настройки параметров',
  HOMEWORK_FORM_RIGTH_PATH_TITLE: 'Настройки пользователей',
  HOMEWORK_TABLE_CREATE_TITLE: 'Создать',
  HOMEWORK_TABLE_SAVE_TITLE: 'Редактировать',
  HOMEWORK_TABLE_CREATE: 'Создать',
  HOMEWORK_TABLE_SAVE: 'Сохранить',
  HOMEWORK_SUCCESS_CREATE_MESSAGE: 'Домашнее задание(я) добавлено(ы)',
  HOMEWORK_SUCCESS_SAVE_MESSAGE: 'Домашнее задание успешно изменено',

  // Book
  // Table
  BOOK_TITLE: 'Задачники',
};
