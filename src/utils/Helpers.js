/**
 * Получаем дату либо строкой либо числовом timestamp
 * и приводим в строке формата Y-m-d
 * @param value timestamp or string date
 * @returns {string} (Y-m-d)
 */

/* eslint-disable import/prefer-default-export */
export const toDate = value => {
  let d;
  if (typeof value === 'string') {
    d = new Date(value);
  } else {
    d = new Date(value * 1000);
  }
  const [day, month] = [d.getDate(), d.getMonth() + 1].map(
    num => (num < 10 ? num.toString().padStart(2, '0') : num)
  );
  const date = [d.getFullYear(), month, day].join('-');
  return date;
};

/*
 * Обновление массива элементов по индексу элемента в массиве
 * @param {array} exchangKeys = [{id:1, ...}, {id:2, ...}]
 * @param {object} payload = {id:2, ..., field: 'additional'}
 * @return {array} [{id:1, ...}, {id:2, ..., field: 'additional'}]
 */
export const updatingElementArray = (exchangKeys, payload) =>
  exchangKeys.map((elem, index) => {
    if (index === payload.itemIndex) {
      return { ...elem, errorMessage: payload.errorMessage };
    }
    return elem;
  });

/**
 * Просто ролучение случайного числа
 * @param {number} min
 * @param {number} max
 * @returns {*}
 */
export const getSimpleRandomInt = (min, max) =>
  Math.floor(Math.random() * (max - min)) + min;
