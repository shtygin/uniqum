import { connect } from 'react-redux';
import { IntlProvider, addLocaleData } from 'react-intl';

import ru from 'react-intl/locale-data/ru';
import en from 'react-intl/locale-data/en';
import zh from 'react-intl/locale-data/zh';

addLocaleData([...ru, ...en, ...zh]);

const mapStateToProps = ({ user: { lang, localeMessages } }) => ({
  locale: lang,
  messages: localeMessages,
});

export default connect(mapStateToProps)(IntlProvider);
