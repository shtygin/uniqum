import { RSAA } from 'redux-api-middleware';
import { REQUEST, SUCCESS, FAILURE } from 'modules/Requests';

const host = process.env.BACKEND_URL;

export const types = {
  SET_DATA: 'ARIFMETIKBOOK/SET_DATA',
  SET_NUMBER: 'SET_NUMBER',
  SET_ROOM: 'SET_ROOM',
  SET_PARAMS: 'SET_PARAMS',
  SET_CONTROLS: 'SET_CONTROLS',
  SET_DICTATION_STATE: 'SET_DICTATION_STATE',
  REQUEST_GET_ARGS: 'ARIFMETIKBOOK/REQUEST_GET_ARGS',
};

// Init state
const initialState = {
  books: {},
  data: [[], [], [], [], [], []],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_DATA:
    case types.REQUEST_GET_ARGS:
      return {
        ...state,
        [action.meta]: action.payload,
      };
    default:
      return state;
  }
};

// Actions
const fetchArgs = (terminalCount = '1', formData) => ({
  [RSAA]: {
    endpoint: `${host}/arifmetik_book/args?terminal_count=${terminalCount}`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: types.REQUEST_GET_ARGS,
      },
    })),
  },
});

const fetchArgsOnce = (number, terminalCount = '1', formData) => ({
  [RSAA]: {
    endpoint: `${host}/arifmetik_book/args?terminal_count=${terminalCount}`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: `${types.REQUEST_GET_ARGS}_${number}`,
      },
    })),
  },
});

export const actions = {
  fetchArgs,
  fetchArgsOnce,
};

export default reducer;
