export const SET_USERS_AVAILABLE = 'socket/SET_USERS_AVAILABLE';
export const MESSAGE_RECEIVED = 'MESSAGE_RECEIVED';
export const SET_ENTRY = 'SET_ENTRY';
export const SET_EVENT = 'SET_EVENT';
export const SET_ROOMS = 'SET_ROOMS';
export const SET_MEMBERS = 'SET_MEMBERS';
export const CHANGE_POSITION = 'CHANGE_POSITION';
export const CHANGE_DICTATION = 'CHANGE_DICTATION';
export const CHANGE_CONTROL = 'CHANGE_CONTROL';
export const CHANGE_DICTATION_SETTING = 'CHANGE_DICTATION_SETTING';
export const CHANGE_REQUESTS = 'CHANGE_REQUESTS';
export const STARTED = 'STARTED';
export const SET_AGRS = 'SET_AGRS';

// const entity = 'WSOCKET';

// Init state
const initialState = {
  usersAvailable: [],
  position: {},
  control: {
    message: {
      start: false,
    },
  },
  rooms: [],
  event: '',
  membersRoom: [],
  requests: {},
  dictation: {},
  // request: {
  //   error: false,
  //   success: false,
  //   loading: false,
  // },
};

// Reducer
const socketReducer = (state = initialState, action) => {
  const { requests } = state;
  switch (action.type) {
    case SET_EVENT:
    case SET_ROOMS:
    case CHANGE_CONTROL:
    case CHANGE_POSITION:
    case CHANGE_DICTATION:
    case SET_USERS_AVAILABLE:
      return {
        ...state,
        [action.meta]: action.payload,
      };
    case SET_MEMBERS:
      return {
        ...state,
        [action.meta]: action.payload.filter(m => m),
      };
    case CHANGE_REQUESTS: {
      // const nextRequests = {...requests};
      return {
        ...state,
        requests: {
          ...requests,
          [action.payload.user._id]: action.payload.statuses,
        },
      };
    }
    default:
      return state;
  }
};

export const types = {
  SOCKET_OPEN: 'SOCKET_OPEN',
  SOCKET_CLOSE: 'SOCKET_CLOSE',
  SOCKET_ERROR: 'SOCKET_ERROR',
  SOCKET_MESSAGE: 'SOCKET_MESSAGE',
  SOCKET_CONNECT: 'SOCKET_CONNECT',
  SOCKET_CONNECTED: 'SOCKET_CONNECTED',
  SOCKET_DISCONNECT: 'SOCKET_DISCONNECT',
  SOCKET_DISCONNECTED: 'SOCKET_DISCONNECTED',
  SOCKET_CONNECTING: 'SOCKET_CONNECTING',
  SEND_MESSAGE: 'SEND_MESSAGE',
  JOIN_ROOM: 'JOIN_ROOM',
  LEAVE_ROOM: 'LEAVE_ROOM',
  USER_REGISTER: 'USER_REGISTER',
  SET_ROOM_HISTORY: 'SET_ROOM_HISTORY',
  GET_ROOMS: 'GET_ROOMS',
  // SET_REQUEST_STATUS: `${entity}/SET_REQUEST`,
  GET_USERS_AVAILABLE: 'GET_USERS_AVAILABLE',
  SET_GLOBAL_FILTER_PROP: 'SET_GLOBAL_FILTER_PROP',
  CHANGE_GLOBAL_CONTROL: 'CHANGE_GLOBAL_CONTROL',
};

export const actions = {
  socketOpen: () => ({ type: types.SOCKET_OPEN }),
  socketClose: () => ({ type: types.SOCKET_CLOSE }),
  socketError: err => ({ type: types.SOCKET_ERROR, payload: err }),
  socketMessage: e => ({
    type: types.SOCKET_MESSAGE,
    payload: JSON.parse(e.data),
  }),
  getUsersAvailable: () => ({ type: types.GET_USERS_AVAILABLE }),
  getRooms: () => ({ type: types.GET_ROOMS }),
  userRegister: payload => ({ type: types.USER_REGISTER, payload }),
  joinRoom: payload => ({ type: types.JOIN_ROOM, payload }),
  leaveRoom: payload => ({ type: types.LEAVE_ROOM, payload }),
  // setRequestStatus: payload => ({
  //   type: types.SET_REQUEST_STATUS,
  //   payload,
  //   meta: 'request',
  // }),
  setMembersRoom: payload => ({
    type: SET_MEMBERS,
    payload,
    meta: 'membersRoom',
  }),
  messageReceived: payload => ({ type: types.MESSAGE_RECEIVED, payload }),
  send: (payload, cb) => ({ type: types.SEND_MESSAGE, payload, cb }),
  socketConnect: () => ({ type: types.SOCKET_CONNECT }),
  connected: () => ({ type: types.SOCKET_CONNECTED }),
  connecting: () => ({ type: types.SOCKET_CONNECTING }),
  disconnect: () => ({ type: types.SOCKET_DISCONNECT }),
  disconnected: () => ({ type: types.SOCKET_DISCONNECTED }),
};

export default socketReducer;
