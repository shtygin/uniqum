export const types = {
  ENQUEUE_SNACKBAR: 'NOTIFIER/ENQUEUE_SNACKBAR',
  REMOVE_SNACKBAR: 'NOTIFIER/REMOVE_SNACKBAR',
};

// Init state
const initialState = {
  notifications: [],
};

// Reducer
const notifierReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.ENQUEUE_SNACKBAR:
      return {
        ...state,
        notifications: [
          ...state.notifications,
          {
            ...action.notification,
          },
        ],
      };

    case types.REMOVE_SNACKBAR:
      return {
        ...state,
        notifications: state.notifications.filter(
          notification => notification.key !== action.key
        ),
      };

    default:
      return state;
  }
};

// Actions
const enqueueSnackbar = notification => ({
  type: types.ENQUEUE_SNACKBAR,
  notification: {
    key: new Date().getTime() + Math.random(),
    ...notification,
  },
});

const removeSnackbar = key => ({
  type: types.REMOVE_SNACKBAR,
  key,
});

export const actions = {
  enqueueSnackbar,
  removeSnackbar,
};

export default notifierReducer;
