import cookie from 'react-cookies';
import languages from 'utils/languages';
import { RSAA } from 'redux-api-middleware';
import { REQUEST, SUCCESS, FAILURE } from 'modules/Requests';

export const SET_LOCALE = 'locale/SET_LOCALE';
export const SET_LOCALE_MESSAGES = 'locale/SET_LOCALE_MESSAGES';

export const REQUEST_CHANGE_LOCALE = 'locale/requestChangeLocale';

const host = process.env.BACKEND_URL;

export const listAvailableLocale = [
  // {
  //   languageId: 'english',
  //   locale: 'en',
  //   name: 'English',
  //   icon: 'en',
  // },
  // {
  //   languageId: 'chinese',
  //   locale: 'zh',
  //   name: 'Chinese',
  //   icon: 'zh',
  // },
  {
    languageId: 'russian',
    locale: 'ru',
    name: 'Russian',
    icon: 'ru',
  },
];

const availableLocale = locale => {
  const found = listAvailableLocale.find(l => l.locale === locale);
  if (found) {
    return found.locale;
  }
  return false;
};

const language =
  (navigator.languages && navigator.languages[0]) ||
  navigator.language ||
  navigator.userLanguage;

const browserLang = language.toLowerCase().split(/[_-]+/)[0];
const cookieLang = cookie.load('language');
export const lang =
  availableLocale(cookieLang) || availableLocale(browserLang) || 'ru';

// Actions
export const setLocale = payload => ({
  type: SET_LOCALE,
  payload,
  meta: 'lang',
});

export const setLocaleMessages = payload => ({
  type: SET_LOCALE_MESSAGES,
  payload: languages[payload],
  meta: 'localeMessages',
});

export const fetchChangeLocale = formData => ({
  [RSAA]: {
    endpoint: `${host}/users/lang`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_CHANGE_LOCALE,
      },
    })),
  },
});
