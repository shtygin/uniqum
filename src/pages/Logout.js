import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import cookie from 'react-cookies';
import { FormattedMessage } from 'react-intl';

import { cleanUser } from 'modules/User';
import SuccessMessage from 'components/SuccessMessage';

const Logout = ({ onLogout }) => {
  onLogout();
  return (
    <div>
      <SuccessMessage
        message={
          <FormattedMessage // eslint-disable-line react/jsx-wrap-multilines
            id="logout.success_message"
            defaultMessage=""
          />
        }
      />
    </div>
  );
};

Logout.propTypes = {
  onLogout: PropTypes.func.isRequired,
};

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  onLogout() {
    cookie.remove('Authorization', { path: '/' });
    dispatch(cleanUser());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Logout);
