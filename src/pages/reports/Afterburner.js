import React from 'react';
import { Bar } from 'react-chartjs-2';
import merge from 'lodash.merge';
import PropTypes from 'prop-types';

const dataDefault = {
  datasets: [
    {
      label: 'Сложность',
      type: 'line',
      data: [],
      fill: false,
      borderColor: '#EC932F',
      backgroundColor: '#EC932F',
      pointBorderColor: '#EC932F',
      pointBackgroundColor: '#EC932F',
      pointHoverBackgroundColor: '#EC932F',
      pointHoverBorderColor: '#EC932F',
      yAxisID: 'y-axis-2',
    },
    {
      type: 'bar',
      label: 'Кол-во примеров',
      data: [],
      fill: false,
      backgroundColor: '#71B37C',
      borderColor: '#71B37C',
      hoverBackgroundColor: '#71B37C',
      hoverBorderColor: '#71B37C',
      yAxisID: 'y-axis-1',
    },
  ],
};

const optionsDefault = {
  responsive: true,
  tooltips: {
    mode: 'label',
  },
  elements: {
    line: {
      fill: false,
    },
  },
  scales: {
    xAxes: [
      {
        display: true,
        gridLines: {
          display: false,
        },
        labels: [],
      },
    ],
    yAxes: [
      {
        type: 'linear',
        display: true,
        position: 'left',
        id: 'y-axis-1',
        gridLines: {
          display: false,
        },
        labels: {
          show: true,
        },
      },
      {
        type: 'linear',
        display: true,
        position: 'right',
        id: 'y-axis-2',
        gridLines: {
          display: false,
        },
        labels: {
          show: true,
        },
      },
    ],
  },
};

/**
 * Afterburner chart.
 * @param {object} values values.
 */
class Afterburner extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    const { data, options } = props;
    this.state = {
      data: merge({ ...dataDefault }, data),
      options: merge({ ...optionsDefault }, options),
    };
  }

  /**
   * Life cycle
   * @param {object} nextProps new props.
   */
  componentWillReceiveProps(nextProps) {
    const { data } = nextProps;
    if (nextProps !== this.props) {
      this.setState({
        data: merge({ ...dataDefault }, data),
      });
    }
  }

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const { data, options } = this.state;

    return <Bar key={Math.random()} data={data} options={options} />;
  }
}

Afterburner.propTypes = {
  data: PropTypes.object.isRequired,
  options: PropTypes.object.isRequired,
};

export default Afterburner;
