import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Bar } from 'react-chartjs-2';
import { simulators, HOMEWORK_FORM } from 'modules/Homework';
import { FormError, H6 } from 'UIKit/Fonts';
import Loading from 'components/Form/Loading';
import { Box } from 'UIKit/Grid';
import { StyledLabel, Label } from 'UIKit/Form';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import RenderCustomSelect from 'components/Form/Input/RenderCustomSelect';
import {
  ROLE,
  fetchReports,
  fetchGroups,
  fetchStudents,
  fetchTeachers,
  fetchSchools,
  REQUEST_GROUPS,
  REQUEST_TEACHERS,
  REQUEST_STUDENTS,
} from 'modules/User';
import Grid from '@material-ui/core/Grid';
import Panel from 'components/Panel';
import { combineRequests } from 'modules/Requests';

/**
 * Report page.
 * @param {object} values values.
 */
class Report extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    const { loadData, user, getReports } = props;
    loadData();
    if (user.user) {
      getReports(user.user._id);
    }
  }

  /**
   * @param {object} nextProps next props.
   */
  componentWillReceiveProps(nextProps) {
    const { getReports, user } = this.props;
    const { user: nextUser } = nextProps;
    const { AUTH } = ROLE;

    if (
      nextUser.role &&
      nextUser.role === AUTH &&
      user.role !== nextUser.role
    ) {
      getReports(nextUser._id);
    }
  }

  /**
   * @param {object} option props.
   */
  handleChange = option => {
    const { getReports } = this.props;
    getReports(option.value);
  };

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const {
      user,
      groups,
      students,
      teachers,
      schools,
      selectedGroup,
      requests,
      reports,
      selectedStudent,
      selectedTeacher,
      selectedSchool,
    } = this.props;
    const { AUTH, ADMIN } = ROLE;
    let groupsList = [];
    let schoolsList = [];
    let studentsList = [];
    let teachersList = [];

    if (teachers.length > 0) {
      teachersList = teachers.map(item => ({
        value: item.id,
        label: item.name,
      }));
    }

    if (schools.length > 0) {
      schoolsList = schools.map(item => {
        let label = item.name;
        if (item.school) {
          label = item.school.name;
        }
        return {
          value: item.id,
          label,
        };
      });
    }

    if (teachers.length > 0) {
      if (selectedSchool.value) {
        teachersList = teachers
          .filter(item => item.creator_id._id === selectedSchool.value)
          .map(item => ({
            value: item.id,
            label: item.name,
          }));
      } else {
        teachersList = teachers.map(item => ({
          value: item.id,
          label: item.name,
        }));
      }
    }

    if (groups.length > 0) {
      if (selectedTeacher.value) {
        groupsList = groups
          .filter(item => item.creator_id._id === selectedTeacher.value)
          .map(item => ({
            value: item.id,
            label: item.name,
          }));
      } else {
        groupsList = groups.map(item => ({
          value: item.id,
          label: item.name,
        }));
      }
    }

    if (students.length > 0) {
      if (selectedGroup.value) {
        studentsList = students
          .filter(item => {
            if (item.groups.filter(j => j._id === selectedGroup.value)[0]) {
              return item;
            }
            return false;
          })
          .map(item => ({
            value: item.id,
            label: [item.name, item.surname].join(' '),
          }));
      } else {
        studentsList = students.map(item => ({
          value: item.id,
          label: [item.name, item.surname].join(' '),
        }));
      }
    }

    let j = 0;

    const request = combineRequests([
      requests[REQUEST_GROUPS],
      requests[REQUEST_STUDENTS],
      requests[REQUEST_TEACHERS],
    ]);
    return (
      <div>
        {request && (
          <div style={{ height: '700px' }}>
            <Box w={[1, 1, 1, 1]} px={4} pb={[4, 8]}>
              <Panel>
                <ul>
                  <li>
                    Статистика показывает решённые задания по всем разделам:
                    домашнее задание и задачи решённые при удаленном обучении.
                  </li>
                  <li>
                    Количество правильных/неправильных ответов указаны на левой
                    вертикальной шкале, медианное значение параметров: интервал,
                    кол-во слагаемых, разряд числа - отмечены на правой
                    вертикальной шкале.
                  </li>
                </ul>
              </Panel>
            </Box>
            {request.loading && <Loading />}
            {request.error && <FormError>request.errorMessage</FormError>}
            {request.success && (
              <Panel>
                <Grid
                  container
                  direction="row"
                  justify="space-between"
                  alignItems="center"
                >
                  {user.role !== AUTH && (
                    <Fragment>
                      {schools && schools.length > 0 && user.role >= ADMIN && (
                        <Box w={[1, 1, 1 / 2, 1 / 4]} px={4} pb={[4, 8]}>
                          <StyledLabel>
                            <Label>Школа</Label>
                            <Field
                              name="school"
                              component={RenderCustomSelect}
                              {...{
                                options: schoolsList,
                                isSearchable: true,
                              }}
                            />
                          </StyledLabel>
                        </Box>
                      )}
                      {teachers && teachers.length > 0 && user.role >= ADMIN && (
                        <Box w={[1, 1, 1 / 2, 1 / 4]} px={4} pb={[4, 8]}>
                          <StyledLabel>
                            <Label>Педагог</Label>
                            <Field
                              name="teacher"
                              component={RenderCustomSelect}
                              {...{
                                options: teachersList,
                                isSearchable: true,
                              }}
                            />
                          </StyledLabel>
                        </Box>
                      )}
                      <Box w={[1, 1, 1 / 2, 1 / 4]} px={4} pb={[4, 8]}>
                        <StyledLabel>
                          <Label>Группа</Label>
                          <Field
                            name="group"
                            component={RenderCustomSelect}
                            {...{
                              options: groupsList,
                              isSearchable: true,
                            }}
                          />
                        </StyledLabel>
                      </Box>
                      <Box w={[1, 1, 1 / 2, 1 / 4]} px={4} pb={[4, 8]}>
                        <StyledLabel>
                          <Label>Ученик</Label>
                          <Field
                            name="student"
                            component={RenderCustomSelect}
                            {...{
                              options: studentsList,
                              isSearchable: true,
                              handleChange: this.handleChange,
                            }}
                          />
                        </StyledLabel>
                      </Box>
                    </Fragment>
                  )}
                  {reports &&
                    reports.datasets &&
                    reports.datasets.map(r => {
                      j += 1;
                      return (
                        <Grid key={j} item xs={6} lg={6} sm={6} md={6}>
                          <H6 style={{ marginLeft: '10px', fontSize: '20' }}>
                            {
                              simulators.filter(
                                item => item.value === r.trainer
                              )[0].label
                            }
                          </H6>
                          <Bar
                            key={selectedStudent}
                            height={300}
                            data={{
                              datasets: r.data,
                              labels: reports.labels,
                            }}
                            options={{
                              maintainAspectRatio: false,
                              scales: {
                                xAxes: [
                                  {
                                    stacked: true,
                                  },
                                ],
                                yAxes: [
                                  {
                                    stacked: true,
                                    display: true,
                                    position: 'left',
                                    id: 'y-axis-1',
                                  },
                                  {
                                    stacked: false,
                                    display: true,
                                    position: 'right',
                                    id: 'y-axis-2',
                                  },
                                ],
                              },
                            }}
                          />
                        </Grid>
                      );
                    })}
                </Grid>
              </Panel>
            )}
          </div>
        )}
      </div>
    );
  }
}

const selector = formValueSelector(HOMEWORK_FORM);
Report.propTypes = {
  requests: PropTypes.object.isRequired,
  loadData: PropTypes.func.isRequired,
  getReports: PropTypes.func.isRequired,
  groups: PropTypes.array.isRequired,
  students: PropTypes.array.isRequired,
  reports: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  teachers: PropTypes.array.isRequired,
  schools: PropTypes.array.isRequired,
  selectedGroup: PropTypes.object.isRequired,
  selectedSchool: PropTypes.object.isRequired,
  selectedTeacher: PropTypes.object.isRequired,
  selectedStudent: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  requests: state.requests,
  user: state.user,
  groups: state.user.groups,
  students: state.user.students,
  reports: state.user.reports,
  teachers: state.user.teachers,
  schools: state.user.schools,
  selectedGroup: selector(state, 'group') ? selector(state, 'group') : {},
  selectedTeacher: selector(state, 'teacher') ? selector(state, 'teacher') : {},
  selectedSchool: selector(state, 'school') ? selector(state, 'school') : {},
  selectedStudent: selector(state, 'student') ? selector(state, 'student') : {},
});

const mapDispatchToProps = dispatch => ({
  loadData() {
    dispatch(fetchTeachers());
    dispatch(fetchStudents());
    dispatch(fetchGroups());
    dispatch(fetchSchools());
  },
  getReports(studentId) {
    dispatch(fetchReports(studentId));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  reduxForm({
    form: HOMEWORK_FORM,
  })(Report)
);
