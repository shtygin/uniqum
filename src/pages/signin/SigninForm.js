import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import Reaptcha from 'reaptcha';
import RenderCustomField from 'components/Form/Input/RenderCustomField';
import { FORM_SIGNIN } from 'modules/Guest';
import { email, required, minLength4 } from 'utils/Validate';
import { FormattedMessage } from 'react-intl';

// Components
import { Box } from 'UIKit/Grid';
import { StyledLink } from 'UIKit/Fonts';
import { FormGroup, StyledButton } from 'UIKit/Form';

const { RECAPTCHA_PUBLIC } = process.env;

/**
 * SigninForm.
 */
class SigninForm extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    this.captcha = React.createRef();
    this.onVerify = this.onVerify.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  /**
   * Функция выполняется если пользователь успешно прошел проверку recaptcha.
   * @param {object} gretoken токен recaptcha.
   * @param {object} params парметры компонента Field.
   */
  onVerify(gretoken, params) {
    const { handleSubmit } = this.props;
    params.input.onChange(gretoken);
    handleSubmit();
    if (this.captcha.current.rendered) {
      this.captcha.current.reset();
    }
  }

  /**
   * Выполняется при submit-е формы, запускает проверку невидимой recaptcha.
   * @param {object} event event.
   */
  handleSubmit(event) {
    event.preventDefault();
    this.captcha.current.execute();
  }

  /**
   * @returns {*}
   */
  render() {
    const { invalid, pristine, submitting } = this.props;

    return (
      <Fragment>
        <form onSubmit={this.handleSubmit}>
          <FormGroup>
            <Field
              name="email"
              component={RenderCustomField}
              type="text"
              label="Email"
              validate={[required, email]}
            />
          </FormGroup>
          <FormGroup>
            <Field
              name="password"
              component={RenderCustomField}
              type="password"
              label="Пароль"
              autocomplete="new-password"
              validate={[required, minLength4]}
            />
          </FormGroup>
          <Field
            name="captcha"
            component={params => (
              <Reaptcha
                ref={this.captcha}
                sitekey={RECAPTCHA_PUBLIC}
                onVerify={gretoken => this.onVerify(gretoken, params)}
                size="invisible"
              />
            )}
          />
          <Box mt={10} pb={2}>
            <StyledButton
              type="submit"
              fullWidth
              disabled={invalid || pristine || submitting}
            >
              <FormattedMessage id="signin.btn_title" defaultMessage="" />
            </StyledButton>
          </Box>
          <Box textAlign="center">
            <StyledLink to="/forgot-password">
              <FormattedMessage id="nav.forgot_password" defaultMessage="" />
            </StyledLink>
          </Box>
        </form>
      </Fragment>
    );
  }
}

SigninForm.propTypes = {
  invalid: PropTypes.bool.isRequired,
  pristine: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm({
  form: FORM_SIGNIN,
})(SigninForm);
