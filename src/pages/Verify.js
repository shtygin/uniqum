import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import { types, actions } from 'modules/Guest';

// Components
import { FormError, StyledLink } from 'UIKit/Fonts';
import Loading from 'components/Form/Loading';

/**
 * Verify page.
 * @param {object} values values.
 */
class Verify extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    const { params } = props.match;
    props.loadData(params.code);
  }

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const { requests } = this.props;
    const request = requests[types.REQUEST_VERIFICATION];

    return (
      <Fragment>
        {request && (
          <Fragment>
            {request.loading && <Loading />}
            {request.error && (
              <FormError>
                <FormattedMessage // eslint-disable-line react/jsx-wrap-multilines
                  id={request.errorMessage}
                  defaultMessage=""
                />
              </FormError>
            )}
            {request.success && (
              <div>
                <FormattedMessage // eslint-disable-line react/jsx-wrap-multilines
                  id="verify.success_message"
                  defaultMessage=""
                />
                <StyledLink to="/">
                  <FormattedMessage // eslint-disable-line react/jsx-wrap-multilines
                    id="verify.success_message_link"
                    defaultMessage=""
                  />
                </StyledLink>
              </div>
            )}
          </Fragment>
        )}
      </Fragment>
    );
  }
}

Verify.propTypes = {
  loadData: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
  requests: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  requests: state.requests,
});

const mapDispatchToProps = dispatch => ({
  loadData(code) {
    dispatch(actions.fetchVerification(code));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Verify);
