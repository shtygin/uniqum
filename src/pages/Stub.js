import React from 'react';
import { Link } from 'react-router-dom';
import Panel from 'components/Panel';

// Components
import { Flex, Box } from 'UIKit/Grid';
import { H6 } from 'UIKit/Fonts';
import { StyledButton } from 'UIKit/Form';

// Styles
import styled from 'styled-components';
import colors from 'UIKit/Colors';

export const LinkStyled = styled(Link)`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  height: 48px;
  padding: 5px 36px;
  border-radius: 2px !important;
  background: linear-gradient(-45deg, #1de9b6 0%, #1dc4e9 100%) !important;
  color: ${colors.white['0']} !important;
  box-shadow: 0 10px 20px 0 rgba(0, 0, 0, 0.15) !important;
  font-size: 14px !important;
  font-weight: bold;
  text-transform: uppercase !important;
  text-decoration: none !important;
  transition: all 0.3s ease-in;
  &:hover {
    box-shadow: 0 10px 20px 0 rgba(0, 0, 0, 0.2) !important;
    transition: none;
  }
`;

const Stub = () => (
  <Flex flexWrap="wrap" mx={-4}>
    <Box w={[1, 1, 1 / 2, 1 / 2]} px={4} pb={[4, 8]}>
      <Panel>
        <Box px={[4, 8]} py={[3, 6]}>
          <Box mb={3}>
            <H6>
              <b>Раздел будет доступен после подключения.</b>
            </H6>
          </Box>
          <Flex
            justifyContent="space-between"
            flexDirection={['column', 'row']}
          >
            <Box mt={3}>
              <LinkStyled to="/payment">Подключиться</LinkStyled>
            </Box>
            <Box mt={3}>
              <StyledButton
                lite="true"
                fullWidth
                onClick={() => {
                  window.location.href = 'http://demo.gorazd.online';
                  return true;
                }}
              >
                Смотреть демонстрацию
              </StyledButton>
            </Box>
          </Flex>
        </Box>
      </Panel>
    </Box>
  </Flex>
);

export default Stub;
