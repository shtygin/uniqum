import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';

// Components
import Table from '@material-ui/core/Table';
import Tooltip from '@material-ui/core/Tooltip';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';

import { Flex, Box } from 'UIKit/Grid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';
import Panel from 'components/Panel';
import { H3, FormError } from 'UIKit/Fonts';

import Button from '@material-ui/core/Button';
import { StyledButtonIcon } from 'UIKit/Form';
import Loading from 'components/Form/Loading';
import { combineRequests } from 'modules/Requests';
import moment from 'moment';
import {
  fetchGroups,
  fetchGroupDelete,
  REQUEST_GROUPS,
  REQUEST_GROUP_DELETE,
  ROLE,
} from 'modules/User';
import Delete from '@material-ui/icons/Delete';
import Edit from '@material-ui/icons/Edit';
import LinkIconStyled from 'components/Layout/LinkIconStyled';

// Styles
import colors from 'UIKit/Colors';
import styled from 'styled-components';

const DATE_FORMAT = 'DD.MM.YYYY';

const StyledTable = styled(Table)`
  font-size: 16px;
`;
const StyledTableCell = styled(TableCell)`
  background-color: ${colors.brand['400']};
  font-size: 10px;
  font-weight: bold;
  letter-spacing: 1.33px;
  color: ${colors.brand['800']} !important;
  text-transform: uppercase;
`;

export const ButtonStyled = styled(Button)`
  width: 72px;
  height: 72px;
  border-radius: 100% !important;
  background-color: ${colors.brand['500']} !important;
  color: ${colors.white['0']} !important;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.16);
  font-size: 28px !important;
  text-decoration: none !important;
`;

// const CustomTableCell = withStyles(() => ({
//   head: {
//     backgroundColor: 'red',
//     color: 'white',
//   },
//   body: {
//     fontSize: 14,
//   },
// }))(TableCell);

/**
 * Groups page.
 * @param {object} values values.
 */
class Groups extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    props.loadData();

    const columns = [
      { name: 'name', title: 'Название' },
      { name: 'description', title: 'Описание' },
      { name: 'start_date', title: 'Дата начала' },
      { name: 'end_date', title: 'Дата завершения' },
      { name: 'edit', title: 'Операции' },
    ];

    this.state = {
      page: 0,
      rowsPerPage: 10,
      columns,
      init: false,
    };
  }

  /**
   * Life cycle
   * @param {object} nextProps nextProps.
   */
  componentWillReceiveProps(nextProps) {
    const { user } = nextProps;
    const { columns, init } = this.state;

    if (!init) {
      if (user.role === ROLE.SYSTEM) {
        columns.splice(0, 0, { name: 'school', title: 'Школа' });
        columns.splice(1, 0, { name: 'teacher', title: 'Педагог' });
        this.setState({ columns, init: true });
      }
      if (user.role === ROLE.ADMIN) {
        columns.splice(0, 0, { name: 'teacher', title: 'Педагог' });
        this.setState({ columns, init: true });
      }
    }
  }

  /**
   * @param {string} id studentId.
   */
  handleDelete = id => {
    const { onDelete } = this.props;
    onDelete(id);
  };

  /**
   * @param {object} event event.
   * @param {string} page page.
   */
  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  /**
   * @param {object} event event.
   */
  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const { requests, groups, user } = this.props;
    const { columns, rowsPerPage, page } = this.state;

    const request = combineRequests([
      requests[REQUEST_GROUPS],
      requests[REQUEST_GROUP_DELETE],
    ]);

    return (
      <Panel style={{ overflow: 'auto' }}>
        <Box px={4} py={(15, 5)}>
          {request && (
            <div>
              <Box px={[0]} py={[0, 4]}>
                <H3>
                  <FormattedMessage
                    id="GROUP_TITLE"
                    defaultMessage="GROUP_TITLE"
                  />
                </H3>
              </Box>
              {request.loading && <Loading />}
              {request.error && <FormError>{request.errorMessage}</FormError>}
              {groups && (
                <Flex flexWrap="wrap" mx={-4} mb={4}>
                  <Box px={4} w={1}>
                    <Panel table>
                      <Box>
                        <StyledTable>
                          <TableHead>
                            <TableRow>
                              {columns.map(column => (
                                <StyledTableCell key={column.name}>
                                  {column.title}
                                </StyledTableCell>
                              ))}
                            </TableRow>
                          </TableHead>
                          <TableBody>
                            {groups
                              .slice(
                                page * rowsPerPage,
                                page * rowsPerPage + rowsPerPage
                              )
                              .map(row => (
                                <TableRow key={row.id}>
                                  {/* <TableCell component="th" scope="row">
                                    {row.id}
                                  </TableCell> */}
                                  {user.role === ROLE.ADMIN && (
                                    <TableCell>
                                      {row.creator_id &&
                                        [
                                          row.creator_id.name,
                                          row.creator_id.surname,
                                          row.creator_id.patronymic,
                                        ].join(' ')}
                                    </TableCell>
                                  )}
                                  {user.role === ROLE.SYSTEM && (
                                    <Fragment>
                                      <TableCell>
                                        {row.creator_id &&
                                          row.creator_id.creator_id &&
                                          row.creator_id.creator_id.school &&
                                          row.creator_id.creator_id.school.name}
                                      </TableCell>
                                      <TableCell>
                                        {row.creator_id &&
                                          [
                                            row.creator_id.name,
                                            row.creator_id.surname,
                                            row.creator_id.patronymic,
                                          ].join(' ')}
                                      </TableCell>
                                    </Fragment>
                                  )}
                                  <TableCell>{row.name}</TableCell>
                                  <TableCell>{row.description}</TableCell>
                                  <TableCell>
                                    {moment(row.start_date).format(DATE_FORMAT)}
                                  </TableCell>
                                  <TableCell>
                                    {moment(row.end_date).format(DATE_FORMAT)}
                                  </TableCell>
                                  <TableCell>
                                    <Tooltip
                                      title="Редактировать данные группы"
                                      placement="right"
                                    >
                                      <LinkIconStyled
                                        to={`/directory/groups/edit/${row._id}`}
                                      >
                                        <Edit />
                                      </LinkIconStyled>
                                    </Tooltip>{' '}
                                    <Tooltip
                                      title="Удалить группу"
                                      placement="right"
                                    >
                                      <StyledButtonIcon
                                        type="button"
                                        onClick={() =>
                                          this.handleDelete(row.id)
                                        }
                                        del="true"
                                      >
                                        <Delete
                                          style={{
                                            color: 'rgba(0, 0, 0, 0.54)',
                                          }}
                                        />
                                      </StyledButtonIcon>
                                    </Tooltip>
                                  </TableCell>
                                </TableRow>
                              ))}
                          </TableBody>
                        </StyledTable>
                      </Box>
                      <TablePagination
                        rowsPerPageOptions={[10, 25, 50, 100]}
                        component="div"
                        count={groups.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        backIconButtonProps={{
                          'aria-label': 'Предыдущая страница',
                        }}
                        nextIconButtonProps={{
                          'aria-label': 'Следующая страница',
                        }}
                        onChangePage={this.handleChangePage}
                        labelRowsPerPage="Строк на странице"
                        labelDisplayedRows={({ from, to, count }) =>
                          `${from}-${to} из ${count}`
                        }
                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                      />
                    </Panel>
                  </Box>
                </Flex>
              )}
            </div>
          )}
          <Flex justifyContent="center" pb={10}>
            <Tooltip title="Добавить группу" placement="right">
              <ButtonStyled
                variant="raised"
                className="text-white"
                aria-label="Добавить группу"
                component={Link}
                onClick={() => console.log()}
                style={{ textDecoration: 'underline' }}
                to="/directory/groups/create"
              >
                <FontAwesomeIcon icon="plus" />
              </ButtonStyled>
            </Tooltip>
          </Flex>
        </Box>
      </Panel>
    );
  }
}

Groups.propTypes = {
  loadData: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  groups: PropTypes.array.isRequired,
  user: PropTypes.object.isRequired,
  requests: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  user: state.user,
  groups: state.user.groups,
  requests: state.requests,
});

const mapDispatchToProps = dispatch => ({
  loadData() {
    dispatch(fetchGroups());
  },
  onDelete(id) {
    dispatch(fetchGroupDelete(id));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Groups);
