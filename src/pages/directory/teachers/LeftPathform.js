import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { Box } from 'UIKit/Grid';
import { H6 } from 'UIKit/Fonts';
import { Field } from 'redux-form';

import { email, required } from 'utils/Validate';
import { StyledLabel, Label, InputStyled } from 'UIKit/Form';
import RenderCustomField from 'components/Form/Input/RenderCustomField';
import RenderCustomSelect from 'components/Form/Input/RenderCustomSelect';

import { fetchSchools, ROLE } from 'modules/User';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAsterisk } from '@fortawesome/free-solid-svg-icons';

/**
 * LeftPathform.
 */
class LeftPathform extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    props.loadData();
  }

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const { schools, user, isNew } = this.props;
    let options = [];

    if (schools.length > 0) {
      options = schools.map(item => {
        let label = item.name;
        if (item.school) {
          label = item.school.name;
        }
        return {
          value: item.id,
          label,
        };
      });
    }

    return (
      <Fragment>
        <Box py={[0, 4]}>
          <H6>
            <FormattedMessage
              id="GROUP_FORM_LEFT_PATH_TITLE"
              defaultMessage="GROUP_FORM_LEFT_PATH_TITLE"
            />
          </H6>
        </Box>
        <Box mb={[4]}>
          <StyledLabel>
            <Label>
              Email <FontAwesomeIcon icon={faAsterisk} size="sm" color="red" />
            </Label>
            <InputStyled
              validate={[email, required]}
              name="email"
              type="text"
              component={RenderCustomField}
            />
          </StyledLabel>
        </Box>
        <Box mb={[4]}>
          <StyledLabel>
            <Label>
              Пароль{' '}
              {isNew && (
                <FontAwesomeIcon icon={faAsterisk} size="sm" color="red" />
              )}
            </Label>
            <InputStyled
              name="password"
              type="text"
              validate={isNew && [required]}
              component={RenderCustomField}
            />
          </StyledLabel>
        </Box>
        {user.role === ROLE.SYSTEM && (
          <Box mb={[4]}>
            <StyledLabel>
              <Label>
                Школа{' '}
                <FontAwesomeIcon icon={faAsterisk} size="sm" color="red" />
              </Label>
              <Field
                name="creator_id"
                validate={[required]}
                component={RenderCustomSelect}
                {...{
                  options,
                  isSearchable: true,
                }}
              />
            </StyledLabel>
          </Box>
        )}
        <Box mb={[4]}>
          <StyledLabel>
            <Label>Имя</Label>
            <InputStyled name="name" type="text" component="input" />
          </StyledLabel>
        </Box>
        <Box mb={[4]}>
          <StyledLabel>
            <Label>Фамилия</Label>
            <InputStyled name="surname" type="text" component="input" />
          </StyledLabel>
        </Box>
        <Box mb={[4]}>
          <StyledLabel>
            <Label>Отчество</Label>
            <InputStyled name="patronymic" type="text" component="input" />
          </StyledLabel>
        </Box>
        <Box mb={[4]}>
          <StyledLabel>
            <Label>Телефон</Label>
            <InputStyled name="phone" type="text" component="input" />
          </StyledLabel>
        </Box>
      </Fragment>
    );
  }
}

LeftPathform.propTypes = {
  loadData: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  schools: PropTypes.array.isRequired,
  isNew: PropTypes.bool.isRequired,
};

const mapDispatchToProps = dispatch => ({
  loadData() {
    dispatch(fetchSchools());
  },
});

const mapStateToProps = state => ({
  user: state.user,
  schools: state.user.schools,
});

export default connect(mapStateToProps, mapDispatchToProps)(LeftPathform);
