import React, { Fragment } from 'react';

import { FormattedMessage } from 'react-intl';
import { Box } from 'UIKit/Grid';
import { H6 } from 'UIKit/Fonts';
import { email } from 'utils/Validate';
import { StyledLabel, Label, InputStyled } from 'UIKit/Form';
import RenderCustomField from 'components/Form/Input/RenderCustomField';

const RigthPathForm = () => (
  <Fragment>
    <Box py={[0, 4]}>
      <H6>
        <FormattedMessage
          id="STUDENT_FORM_RIGTH_PATH_TITLE"
          defaultMessage="STUDENT_FORM_RIGTH_PATH_TITLE"
        />
      </H6>
    </Box>
    <Box mb={[4]}>
      <StyledLabel>
        <Label>Email</Label>
        <InputStyled
          validate={[email]}
          name="parent[email]"
          type="text"
          component={RenderCustomField}
        />
      </StyledLabel>
    </Box>
    <Box mb={[4]}>
      <StyledLabel>
        <Label>Имя</Label>
        <InputStyled name="parent[name]" type="text" component="input" />
      </StyledLabel>
    </Box>
    <Box mb={[4]}>
      <StyledLabel>
        <Label>Отчество</Label>
        <InputStyled name="parent[surname]" type="text" component="input" />
      </StyledLabel>
    </Box>
    <Box mb={[4]}>
      <StyledLabel>
        <Label>Фамилия</Label>
        <InputStyled name="parent[patronymic]" type="text" component="input" />
      </StyledLabel>
    </Box>
    <Box mb={[4]}>
      <StyledLabel>
        <Label>Телефон</Label>
        <InputStyled name="parent[phone]" type="text" component="input" />
      </StyledLabel>
    </Box>
  </Fragment>
);

export default RigthPathForm;
