import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import {
  fetchStudentCreate,
  STUDENT_FORM,
  REQUEST_STUDENT_CREATE,
} from 'modules/User';
import { FormattedMessage } from 'react-intl';

// Components
import { Box } from 'UIKit/Grid';
import Panel from 'components/Panel';
import { H3, FormError, FormSuccess } from 'UIKit/Fonts';

import Loading from 'components/Form/Loading';

import colors from 'UIKit/Colors';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import StudentForm from './StudentForm';

export const ButtonStyled = styled(Button)`
  background-color: ${colors.brand['500']} !important;
  color: ${colors.white['0']} !important;
`;

/**
 * CreateStudent page.
 * @param {object} values values.
 */
class CreateStudent extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    this.state = {
      query: false,
    };
  }

  /**
   * @param {object} formData formData.
   */
  onSubmit = formData => {
    const { onCreate } = this.props;
    this.setState({ query: true }, () => onCreate(formData));
  };

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const { requests, handleSubmit } = this.props;
    const { query } = this.state;
    const request = requests[REQUEST_STUDENT_CREATE];
    return (
      <Box w={1} px={4}>
        <Panel>
          <Box px={[1, 8]} py={[0, 4]}>
            <H3>
              {request && query && (
                <Fragment>
                  {request.loading && <Loading />}

                  {request.error && (
                    <FormError>
                      <FormattedMessage
                        id={request.errorMessage}
                        defaultMessage={request.errorMessage}
                      />
                    </FormError>
                  )}
                  {request.data && <FormSuccess>Ученик добавлен</FormSuccess>}
                </Fragment>
              )}
              <FormattedMessage
                id="STUDENT_TABLE_CREATE_TITLE"
                defaultMessage="STUDENT_TABLE_CREATE_TITLE"
              />
            </H3>
          </Box>
          <Box px={[4, 8]} py={[3, 6]}>
            <form onSubmit={handleSubmit(this.onSubmit)}>
              <StudentForm isNew />
              <ButtonStyled fullWidth type="submit">
                <FormattedMessage
                  id="STUDENT_TABLE_CREATE"
                  defaultMessage="STUDENT_TABLE_CREATE"
                />
              </ButtonStyled>
            </form>
          </Box>
        </Panel>
      </Box>
    );
  }
}

CreateStudent.propTypes = {
  onCreate: PropTypes.func.isRequired,
  requests: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

const onSubmit = (formData, dispatch) => {
  dispatch(fetchStudentCreate(formData));
};

const mapDispatchToProps = dispatch => ({
  onCreate(formData) {
    dispatch(fetchStudentCreate(formData));
  },
});

const mapStateToProps = state => ({
  requests: state.requests,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  reduxForm({
    form: STUDENT_FORM,
    onSubmit,
  })(CreateStudent)
);
