import React from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';

// Components
import Table from '@material-ui/core/Table';
import Tooltip from '@material-ui/core/Tooltip';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';

import { Flex, Box } from 'UIKit/Grid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';
import Panel from 'components/Panel';
import { H3, FormError } from 'UIKit/Fonts';

import Button from '@material-ui/core/Button';
import { StyledButtonIcon } from 'UIKit/Form';
import Loading from 'components/Form/Loading';
import { combineRequests } from 'modules/Requests';
import {
  fetchSchools,
  fetchSchoolDelete,
  REQUEST_SCHOOLS,
  REQUEST_SCHOOL_DELETE,
} from 'modules/User';
import Delete from '@material-ui/icons/Delete';
import Edit from '@material-ui/icons/Edit';
import LinkIconStyled from 'components/Layout/LinkIconStyled';

// Styles
import colors from 'UIKit/Colors';
import styled from 'styled-components';

const StyledTable = styled(Table)`
  font-size: 16px;
`;
const StyledTableCell = styled(TableCell)`
  background-color: ${colors.brand['400']};
  font-size: 10px;
  font-weight: bold;
  letter-spacing: 1.33px;
  color: ${colors.brand['800']} !important
  text-transform: uppercase;
`;

export const ButtonStyled = styled(Button)`
  width: 72px;
  height: 72px;
  border-radius: 100% !important;
  background-color: ${colors.brand['500']} !important;
  color: ${colors.white['0']} !important;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.16);
  font-size: 28px !important;
  text-decoration: none !important;
`;

/**
 * Schools page.
 * @param {object} values values.
 */
class Schools extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    props.loadData();

    this.state = {
      page: 0,
      rowsPerPage: 10,
      columns: [
        // { name: 'id', title: 'Номер' },
        { name: 'name', title: 'Название' },
        { name: 'city', title: 'Город' },
        { name: 'address', title: 'Адрес' },
        { name: 'metro_station', title: 'Станция метро' },
        { name: 'fio', title: 'ФИО руководителя' },
        { name: 'email', title: 'Email' },
        { name: 'phone', title: 'Телефон' },
        { name: 'edit', title: 'Операции' },
      ],
    };
  }

  /**
   * @param {string} id studentId.
   */
  handleDelete = id => {
    const { onDelete } = this.props;
    onDelete(id);
  };

  /**
   * @param {object} event event.
   * @param {string} page page.
   */
  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  /**
   * @param {object} event event.
   */
  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const { requests, schools } = this.props;
    const { columns, rowsPerPage, page } = this.state;

    const request = combineRequests([
      requests[REQUEST_SCHOOLS],
      requests[REQUEST_SCHOOL_DELETE],
    ]);

    return (
      <Panel style={{ overflow: 'auto' }}>
        <Box px={4} py={(15, 5)}>
          {request && (
            <div>
              <Box px={[0]} py={[0, 4]}>
                <H3>
                  <FormattedMessage
                    id="SCHOOL_TITLE"
                    defaultMessage="SCHOOL_TITLE"
                  />
                </H3>
              </Box>
              {request.loading && <Loading />}
              {request.error && <FormError>{request.errorMessage}</FormError>}
              {schools && (
                <Flex flexWrap="wrap" mx={-4} mb={4}>
                  <Box px={4} w={1}>
                    <Panel table>
                      <Box>
                        <StyledTable>
                          <TableHead>
                            <TableRow>
                              {columns.map(column => (
                                <StyledTableCell key={column.name}>
                                  {column.title}
                                </StyledTableCell>
                              ))}
                            </TableRow>
                          </TableHead>
                          <TableBody>
                            {schools
                              .slice(
                                page * rowsPerPage,
                                page * rowsPerPage + rowsPerPage
                              )
                              .map(row => (
                                <TableRow key={row.id}>
                                  {/* <TableCell component="th" scope="row">
                                    {row.id}
                                  </TableCell> */}
                                  <TableCell>
                                    {row.school && row.school.name}
                                  </TableCell>
                                  <TableCell>
                                    {row.school && row.school.city}
                                  </TableCell>
                                  <TableCell>
                                    {row.school && row.school.address}
                                  </TableCell>
                                  <TableCell>
                                    {row.school && row.school.metro_station}
                                  </TableCell>
                                  <TableCell>
                                    {[
                                      row.name,
                                      row.surname,
                                      row.patronymic,
                                    ].join(' ')}
                                  </TableCell>
                                  <TableCell>{row.email}</TableCell>
                                  <TableCell>{row.phone}</TableCell>
                                  <TableCell>
                                    <Tooltip
                                      title="Редактировать данные школы"
                                      placement="right"
                                    >
                                      <LinkIconStyled
                                        to={`/directory/schools/edit/${row._id}`}
                                      >
                                        <Edit />
                                      </LinkIconStyled>
                                    </Tooltip>{' '}
                                    <Tooltip
                                      title="Удалить школу"
                                      placement="right"
                                    >
                                      <StyledButtonIcon
                                        type="button"
                                        onClick={() =>
                                          this.handleDelete(row.id)
                                        }
                                        del="true"
                                      >
                                        <Delete
                                          style={{
                                            color: 'rgba(0, 0, 0, 0.54)',
                                          }}
                                        />
                                      </StyledButtonIcon>
                                    </Tooltip>
                                  </TableCell>
                                </TableRow>
                              ))}
                          </TableBody>
                        </StyledTable>
                      </Box>
                      <TablePagination
                        rowsPerPageOptions={[10, 25, 50, 100]}
                        component="div"
                        count={schools.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        backIconButtonProps={{
                          'aria-label': 'Предыдущая страница',
                        }}
                        nextIconButtonProps={{
                          'aria-label': 'Следующая страница',
                        }}
                        onChangePage={this.handleChangePage}
                        labelRowsPerPage="Строк на странице"
                        labelDisplayedRows={({ from, to, count }) =>
                          `${from}-${to} из ${count}`
                        }
                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                      />
                    </Panel>
                  </Box>
                </Flex>
              )}
            </div>
          )}
          <Flex justifyContent="center" pb={10}>
            <Tooltip title="Добавить школу" placement="right">
              <ButtonStyled
                variant="raised"
                className="text-white"
                aria-label="Добавить школу"
                component={Link}
                onClick={() => console.log()}
                style={{ textDecoration: 'underline' }}
                to="/directory/schools/create"
              >
                <FontAwesomeIcon icon="plus" />
              </ButtonStyled>
            </Tooltip>
          </Flex>
        </Box>
      </Panel>
    );
  }
}

Schools.propTypes = {
  loadData: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  schools: PropTypes.array.isRequired,
  requests: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  schools: state.user.schools,
  requests: state.requests,
});

const mapDispatchToProps = dispatch => ({
  loadData() {
    dispatch(fetchSchools());
  },
  onDelete(id) {
    dispatch(fetchSchoolDelete(id));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Schools);
