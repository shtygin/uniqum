import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import {
  fetchGroupCreate,
  GROUP_FORM,
  REQUEST_GROUP_CREATE,
} from 'modules/User';
import { FormattedMessage } from 'react-intl';

// Components
import { Box } from 'UIKit/Grid';
import Panel from 'components/Panel';
import { H3, FormError, FormSuccess } from 'UIKit/Fonts';

import Loading from 'components/Form/Loading';

import colors from 'UIKit/Colors';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import GroupForm from './GroupForm';

export const ButtonStyled = styled(Button)`
  background-color: ${colors.brand['500']} !important;
  color: ${colors.white['0']} !important;
`;

/**
 * CreateGroup page.
 * @param {object} values values.
 */
class CreateGroup extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    this.state = {
      query: false,
    };
  }

  /**
   * @param {object} formData formData.
   */
  onSubmit = formData => {
    const { onCreate } = this.props;
    this.setState({ query: true }, () => onCreate(formData));
  };

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const { requests, handleSubmit } = this.props;
    const { query } = this.state;
    const request = requests[REQUEST_GROUP_CREATE];
    return (
      <Box w={1} px={4}>
        <Panel>
          <Box px={[1, 8]} py={[0, 4]}>
            <H3>
              {request && query && (
                <Fragment>
                  {request.loading && <Loading />}
                  {request.error && (
                    <FormError>
                      {' '}
                      <FormattedMessage
                        id={request.errorMessage}
                        defaultMessage={request.errorMessage}
                      />
                    </FormError>
                  )}
                  {request.data && (
                    <FormSuccess>
                      <FormattedMessage
                        id="GROUP_SUCCESS_CREATE_MESSAGE"
                        defaultMessage="GROUP_SUCCESS_CREATE_MESSAGE"
                      />
                    </FormSuccess>
                  )}
                </Fragment>
              )}
              <FormattedMessage
                id="GROUP_TABLE_CREATE_TITLE"
                defaultMessage="GROUP_TABLE_CREATE_TITLE"
              />
            </H3>
          </Box>
          <Box px={[4, 8]} py={[3, 6]}>
            <form onSubmit={handleSubmit(this.onSubmit)}>
              <GroupForm />
              <ButtonStyled fullWidth type="submit">
                <FormattedMessage
                  id="GROUP_TABLE_CREATE"
                  defaultMessage="GROUP_TABLE_CREATE"
                />
              </ButtonStyled>
            </form>
          </Box>
        </Panel>
      </Box>
    );
  }
}

CreateGroup.propTypes = {
  requests: PropTypes.object.isRequired,
  onCreate: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  onCreate(formData) {
    dispatch(fetchGroupCreate(formData));
  },
});

const mapStateToProps = state => ({
  requests: state.requests,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  reduxForm({
    form: GROUP_FORM,
    // onSubmit,
  })(CreateGroup)
);
