import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { Box } from 'UIKit/Grid';
import { H6 } from 'UIKit/Fonts';
import { Field } from 'redux-form';
import { required } from 'utils/Validate';

import { StyledLabel, Label, InputStyled, TextareaStyled } from 'UIKit/Form';
import RenderCustomSelect from 'components/Form/Input/RenderCustomSelect';

import { ROLE, fetchTeachers } from 'modules/User';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAsterisk } from '@fortawesome/free-solid-svg-icons';

/**
 * LeftPathform.
 */
class LeftPathform extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    props.loadData();
  }

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const { user, teachers } = this.props;
    let optionsTeachers = [];

    if (teachers.length > 0) {
      optionsTeachers = teachers.map(item => ({
        value: item.id,
        label: [
          item.surname,
          `${String(item.name).charAt(0)}.`,
          `${String(item.patronymic).charAt(0)}.`,
        ].join(' '),
      }));
    }

    return (
      <Fragment>
        <Box py={[0, 4]}>
          <H6>
            <FormattedMessage
              id="GROUP_FORM_LEFT_PATH_TITLE"
              defaultMessage="GROUP_FORM_LEFT_PATH_TITLE"
            />
          </H6>
        </Box>
        <Box mb={[4]}>
          <StyledLabel>
            <Label>Название</Label>
            <InputStyled name="name" type="text" component="input" />
          </StyledLabel>
        </Box>
        {[ROLE.SYSTEM, ROLE.ADMIN].indexOf(user.role) > -1 && (
          <Box mb={[4]}>
            <StyledLabel>
              <Label>
                Педагог{' '}
                <FontAwesomeIcon icon={faAsterisk} size="sm" color="red" />
              </Label>
              <Field
                name="creator_id"
                validate={[required]}
                component={RenderCustomSelect}
                {...{
                  options: optionsTeachers,
                  isSearchable: true,
                }}
              />
            </StyledLabel>
          </Box>
        )}
        <Box mb={[4]}>
          <StyledLabel>
            <Label>Описание</Label>
            <TextareaStyled name="description" component="textarea" />
          </StyledLabel>
        </Box>
        <Box mb={[4]}>
          <StyledLabel>
            <Label>Дата начала</Label>
            <InputStyled name="start_date" type="date" component="input" />
          </StyledLabel>
        </Box>
        <Box mb={[4]}>
          <StyledLabel>
            <Label>Дата окончания</Label>
            <InputStyled name="end_date" type="date" component="input" />
          </StyledLabel>
        </Box>
      </Fragment>
    );
  }
}

LeftPathform.propTypes = {
  loadData: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  teachers: PropTypes.array.isRequired,
};

const mapDispatchToProps = dispatch => ({
  loadData() {
    dispatch(fetchTeachers());
  },
});

const mapStateToProps = state => ({
  user: state.user,
  teachers: state.user.teachers,
});

export default connect(mapStateToProps, mapDispatchToProps)(LeftPathform);
