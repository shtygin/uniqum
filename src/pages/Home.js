import React from 'react';
import Panel from 'components/Panel';
import { Box } from 'UIKit/Grid';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

const styles = {
  img: {
    margin: 'auto',
    display: 'block',
    width: '100%',
  },
};

const Home = ({ classes }) => (
  <Panel>
    <Box px={4} py={(15, 5)}>
      <img
        alt="Приветсвие"
        className={classes.img}
        src="/assets/img/main.jpg"
      />
    </Box>
  </Panel>
);

Home.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Home);
