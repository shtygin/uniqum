import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import { types, actions } from 'modules/Guest';
import SuccessMessage from 'components/SuccessMessage';

// Components
import { FormError } from 'UIKit/Fonts';
import Loading from 'components/Form/Loading';

/**
 * Resend page.
 * @param {object} values values.
 */
class Resend extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    const { params } = props.match;
    props.loadData(params.email);
  }

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const { requests } = this.props;
    const request = requests[types.REQUEST_RESEND];

    return (
      <Fragment>
        {request && (
          <Fragment>
            {request.loading && <Loading />}
            {request.error && (
              <FormError>
                <FormattedMessage id={request.errorMessage} defaultMessage="" />
              </FormError>
            )}
            {request.success && (
              <SuccessMessage
                message={
                  <FormattedMessage // eslint-disable-line react/jsx-wrap-multilines
                    id="resend.success_message"
                    defaultMessage=""
                  />
                }
              />
            )}
          </Fragment>
        )}
      </Fragment>
    );
  }
}

Resend.propTypes = {
  loadData: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
  requests: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  requests: state.requests,
});

const mapDispatchToProps = dispatch => ({
  loadData(email) {
    dispatch(actions.fetchResend(email));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Resend);
