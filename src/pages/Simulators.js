import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import ChatroomPreview from 'pages/simulators/ChatroomPreview';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  avatar: {
    width: 160,
    height: 160,
    backgroundColor: 'teal',
  },
  icon: {
    width: 100,
    height: 100,
  },
};

const Simulators = props => {
  const { chatrooms } = props;

  return (
    <Grid container justify="center" spacing={10}>
      <Grid item xs={12} lg={10} sm={12} md={10} key={1}>
        <div>
          {chatrooms.map(chatroom => (
            <ChatroomPreview key={chatroom.name} chatroom={chatroom} />
          ))}
        </div>
      </Grid>
    </Grid>
  );
};

Simulators.propTypes = {
  chatrooms: PropTypes.array.isRequired,
};

const mapStateToProps = state => ({
  chatrooms: state.socket.rooms,
});

export default withStyles(styles)(connect(mapStateToProps)(Simulators));
