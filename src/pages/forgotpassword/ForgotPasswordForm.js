import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import { Field, reduxForm } from 'redux-form';
import Reaptcha from 'reaptcha';
import { FormattedMessage } from 'react-intl';

import RenderCustomField from 'components/Form/Input/RenderCustomField';
import { required, email } from 'utils/Validate';

// styled
import { FormGroup } from 'UIKit/Form';
import colors from 'UIKit/Colors';
import styled from 'styled-components';

export const ButtonStyled = styled(Button)`
  background-color: ${colors.brand['500']} !important;
  color: ${colors.white['0']} !important;
`;

const { RECAPTCHA_PUBLIC } = process.env;

/**
 * ForgotPasswordForm.
 */
class ForgotPasswordForm extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    this.captcha = React.createRef();
    this.onVerify = this.onVerify.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  /**
   * Функция выполняется если пользователь успешно прошел проверку recaptcha.
   * @param {object} gretoken токен recaptcha.
   * @param {object} params парметры компонента Field.
   */
  onVerify(gretoken, params) {
    const { handleSubmit } = this.props;
    params.input.onChange(gretoken);
    handleSubmit();
    if (this.captcha.current.rendered) {
      this.captcha.current.reset();
    }
  }

  /**
   * Выполняется при submit-е формы, запускает проверку невидимой recaptcha.
   * @param {object} event event.
   */
  handleSubmit(event) {
    event.preventDefault();
    this.captcha.current.execute();
  }

  /**
   * @returns {*}
   */
  render() {
    const { invalid, pristine, submitting } = this.props;

    return (
      <Fragment>
        <form onSubmit={this.handleSubmit}>
          <FormGroup>
            <Field
              name="email"
              component={RenderCustomField}
              type="text"
              label="Email"
              validate={[required, email]}
            />
          </FormGroup>
          <Field
            name="captcha"
            component={params => (
              <Reaptcha
                ref={this.captcha}
                sitekey={RECAPTCHA_PUBLIC}
                onVerify={gretoken => this.onVerify(gretoken, params)}
                size="invisible"
              />
            )}
          />
          <ButtonStyled
            fullWidth
            type="submit"
            disabled={invalid || pristine || submitting}
          >
            <FormattedMessage id="forgotPassword.btn_title" defaultMessage="" />
          </ButtonStyled>
        </form>
      </Fragment>
    );
  }
}

ForgotPasswordForm.propTypes = {
  invalid: PropTypes.bool.isRequired,
  pristine: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm({
  form: 'forgot',
})(ForgotPasswordForm);
