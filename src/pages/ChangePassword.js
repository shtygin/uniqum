import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import { fetchChangePassword, REQUEST_CHANGE_PASSWORD } from 'modules/User';
import SuccessMessage from 'components/SuccessMessage';

// Components
import Panel from 'components/Panel';
import { H1Panel, FormError } from 'UIKit/Fonts';
import { Flex, Box } from 'UIKit/Grid';
import Loading from 'components/Form/Loading';
import ChangePasswordForm from './changepassword/ChangePasswordForm';

/**
 * ForgotPassword page.
 * @param {object} values values.
 */
class ChangePassword extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  /**
   * @param {object} formData данные формы.
   */
  handleSubmit(formData) {
    const { changePassword } = this.props;
    changePassword(formData);
  }

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const { requests } = this.props;
    const request = requests[REQUEST_CHANGE_PASSWORD];

    return (
      <Fragment>
        <Flex justifyContent="center" alignItems="center" w={1} height="100%">
          <Box w={[1, 10 / 12, 7 / 12]}>
            <Panel>
              <Box px={[8, 14, 24]} py={8}>
                <Box mb={8}>
                  <Box mb={1}>
                    <H1Panel>
                      <FormattedMessage
                        id="changePassword.title"
                        defaultMessage=""
                      />
                    </H1Panel>
                  </Box>
                </Box>
                {request && (
                  <div>
                    {request.loading && <Loading />}
                    {request.error && (
                      <FormError>
                        <FormattedMessage
                          id={request.errorMessage}
                          defaultMessage=""
                        />
                      </FormError>
                    )}
                  </div>
                )}
                {request && request.success ? (
                  <SuccessMessage
                    message={
                      <FormattedMessage // eslint-disable-line react/jsx-wrap-multilines
                        id="changePassword.success_message"
                        defaultMessage=""
                      />
                    }
                  />
                ) : (
                  <ChangePasswordForm onSubmit={this.handleSubmit} />
                )}
              </Box>
            </Panel>
          </Box>
        </Flex>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  requests: state.requests,
});

const mapDispatchToProps = dispatch => ({
  changePassword(formData) {
    dispatch(fetchChangePassword(formData));
  },
});

ChangePassword.propTypes = {
  requests: PropTypes.object.isRequired,
  changePassword: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword);
