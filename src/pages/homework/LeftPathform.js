import React, { Fragment, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { injectIntl, FormattedMessage } from 'react-intl';
import { Box, Flex } from 'UIKit/Grid';
import { H6 } from 'UIKit/Fonts';
import {
  numbers,
  leftNumbers,
  rightNumbers,
  SIGN_ADD,
  SIGN_SUB,
  SIGN_MIX,
  NO_MODE,
  NOT_EXCEED_50,
  NOT_EXCEED_100,
  EXCEED_100,
  RULE_SIMPLE_SUM,
  RULE_FIVE_SUM,
  RULE_TEN_SUM,
  RULE_FIVE_AND_TEN_SUM,
  SECTION_UP_TO_FOUR,
  SECTION_UP_TO_FIVE,
  SECTION_UP_TO_SIX,
  SECTION_UP_TO_SEVEN,
  SECTION_UP_TO_EIGHT,
  SECTION_UP_TO_NINE,
  SECTION_MIRROR,
  SECTION_TEN,
} from 'modules/Simulators';
import { HOMEWORK_FORM } from 'modules/Homework';
import { StyledLabel, Label, InputStyled } from 'UIKit/Form';
import { Field, formValueSelector, change } from 'redux-form';
import { required } from 'utils/Validate';
import RenderCustomSelect from 'components/Form/Input/RenderCustomSelect';

const LeftPathform = ({
  rooms,
  intl,
  selectedMode,
  selectedNumbers,
  selectedSimulator,
  onChangeNumbers,
}) => {
  const options = [
    {
      value: RULE_SIMPLE_SUM,
      label: intl.formatMessage({ id: 'RULE_SIMPLE_SUM' }),
    },
    {
      value: RULE_FIVE_SUM,
      label: intl.formatMessage({ id: 'RULE_FIVE_SUM' }),
    },
    {
      value: RULE_TEN_SUM,
      label: intl.formatMessage({ id: 'RULE_TEN_SUM' }),
    },
    {
      value: RULE_FIVE_AND_TEN_SUM,
      label: intl.formatMessage({ id: 'RULE_FIVE_AND_TEN_SUM' }),
    },
    {
      value: NO_MODE,
      label: intl.formatMessage({ id: 'NO_MODE' }),
    },
  ];

  const optionsSection = [
    {
      value: SECTION_UP_TO_FOUR,
      label: intl.formatMessage({ id: 'SECTION_UP_TO_FOUR' }),
    },
    {
      value: SECTION_UP_TO_FIVE,
      label: intl.formatMessage({ id: 'SECTION_UP_TO_FIVE' }),
    },
    {
      value: SECTION_UP_TO_SIX,
      label: intl.formatMessage({ id: 'SECTION_UP_TO_SIX' }),
    },
    {
      value: SECTION_UP_TO_SEVEN,
      label: intl.formatMessage({ id: 'SECTION_UP_TO_SEVEN' }),
    },
    {
      value: SECTION_UP_TO_EIGHT,
      label: intl.formatMessage({ id: 'SECTION_UP_TO_EIGHT' }),
    },
    {
      value: SECTION_UP_TO_NINE,
      label: intl.formatMessage({ id: 'SECTION_UP_TO_NINE' }),
    },
    {
      value: SECTION_MIRROR,
      label: intl.formatMessage({ id: 'SECTION_MIRROR' }),
    },
    {
      value: SECTION_TEN,
      label: intl.formatMessage({ id: 'SECTION_TEN' }),
    },
  ];

  const optionsSign = [
    {
      value: SIGN_ADD,
      label: '+',
    },
    {
      value: SIGN_SUB,
      label: '-',
    },
    {
      value: SIGN_MIX,
      label: '-/+',
    },
  ];

  const optionsBound = [
    {
      value: NOT_EXCEED_50,
      label: 'Без перехода',
    },
    {
      value: NOT_EXCEED_100,
      label: 'Переход за 50',
    },
    {
      value: EXCEED_100,
      label: 'Переход за 100',
    },
  ];

  const optionsRange = [
    {
      value: 1,
      label: 1,
    },
    {
      value: 2,
      label: 2,
    },

    {
      value: 3,
      label: 3,
    },
  ];

  let optionsRooms = [];

  if (rooms.length > 0) {
    optionsRooms = rooms.map(item => ({
      value: item._id,
      label: item.desc,
    }));
  }

  const [numbersList, setNumbers] = useState(numbers);

  useEffect(() => {
    let nextNumbers = numbers;
    if (Number(selectedMode.value) === 2) {
      const diff = leftNumbers.filter(x =>
        selectedNumbers.map(n => Number(n.value)).includes(x.value)
      );

      setNumbers(leftNumbers);
      if (diff.length === 0) {
        nextNumbers = leftNumbers;
      } else {
        nextNumbers = diff;
      }
    }
    if (Number(selectedMode.value) === 4) {
      const diff = rightNumbers.filter(x =>
        selectedNumbers.map(n => Number(n.value)).includes(x.value)
      );
      setNumbers(rightNumbers);
      if (diff.length === 0) {
        nextNumbers = rightNumbers;
      } else {
        nextNumbers = diff;
      }
    }
    if ([0, 1, 3].indexOf(+selectedMode.value) > -1) {
      setNumbers(numbers);
    }

    onChangeNumbers(nextNumbers);
  }, [selectedMode]);

  const simulator = rooms.filter(i => i._id === selectedSimulator.value);
  const simulatorItem = simulator.length > 0 ? simulator[0] : {};

  return (
    <Fragment>
      <Box py={[0, 4]}>
        <H6>
          <FormattedMessage
            id="HOMEWORK_FORM_LEFT_PATH_TITLE"
            defaultMessage="HOMEWORK_FORM_LEFT_PATH_TITLE"
          />
        </H6>
      </Box>
      {rooms.length > 0 && (
        <Box mb={[6]}>
          <StyledLabel>
            <Label>Тип тренажера</Label>
            <Field
              name="simulator"
              validate={[required]}
              component={RenderCustomSelect}
              {...{
                options: optionsRooms,
                isSearchable: true,
              }}
            />
          </StyledLabel>
        </Box>
      )}
      <Box mb={[6]}>
        <StyledLabel>
          <Label>Правило</Label>
          <Field
            name="params[mode]"
            validate={[required]}
            component={RenderCustomSelect}
            {...{
              options,
            }}
          />
        </StyledLabel>
      </Box>
      {selectedMode && selectedMode.value === 1 && (
        <Box mb={[6]}>
          <StyledLabel>
            <Label>Раздел</Label>
            <Field
              name="params[section]"
              validate={[required]}
              component={RenderCustomSelect}
              {...{
                options: optionsSection,
              }}
            />
          </StyledLabel>
        </Box>
      )}

      {selectedMode && selectedMode.value !== 1 && (
        <Fragment>
          <Box mb={[6]}>
            <StyledLabel>
              <Label>Формула(ы) на число(а):</Label>
              <Field
                name="params[numbers]"
                validate={[required]}
                component={RenderCustomSelect}
                {...{
                  options: numbersList,
                  isMulti: true,
                  closeMenuOnSelect: false,
                }}
              />
            </StyledLabel>
          </Box>
          <Box mb={[6]}>
            <StyledLabel>
              <Label>Знак</Label>
              <Field
                name="params[sign]"
                validate={[required]}
                component={RenderCustomSelect}
                {...{
                  options: optionsSign,
                }}
              />
            </StyledLabel>
          </Box>
        </Fragment>
      )}
      <Box mb={[4]}>
        <StyledLabel>
          <Label>Кол-во слагаемых</Label>
          <InputStyled
            max={30}
            min={1}
            name="params[steps]"
            type="number"
            component="input"
          />
        </StyledLabel>
      </Box>

      <Flex
        alignItems="center"
        justifyContent="space-between"
        flexDirection={['column-reverse', 'row']}
      >
        <Box>
          <StyledLabel>
            <Label>Разряд числа</Label>
          </StyledLabel>
        </Box>
      </Flex>
      <Flex
        alignItems="center"
        justifyContent="space-between"
        flexDirection={['column-reverse', 'row']}
      >
        <Box mb={[4]} w={[1 / 2, 1 / 2, 1 / 2, 1 / 2]}>
          <StyledLabel>
            <Label>от</Label>
            <Field
              name="params[under]"
              validate={[required]}
              component={RenderCustomSelect}
              {...{
                options: optionsRange,
              }}
            />
          </StyledLabel>
        </Box>
        <Box mb={[4]} px={[1, 1]} py={1} w={[1 / 2, 1 / 2, 1 / 2, 1 / 2]}>
          <StyledLabel>
            <Label>до</Label>
            <Field
              name="params[over]"
              validate={[required]}
              component={RenderCustomSelect}
              {...{
                options: optionsRange,
              }}
            />
          </StyledLabel>
        </Box>
      </Flex>
      {simulatorItem.name !== 'roomB' && (
        <Box mb={[4]}>
          <StyledLabel>
            <Label>Временной интервал</Label>
            <InputStyled
              max={30}
              min={0.1}
              step={0.1}
              name="params[interval]"
              type="number"
              component="input"
            />
          </StyledLabel>
        </Box>
      )}
      {selectedMode && (selectedMode.value === 3 || selectedMode.value === 4) && (
        <Box mb={[6]}>
          <StyledLabel>
            <Label>Переход</Label>
            <Field
              name="params[bound]"
              validate={[required]}
              component={RenderCustomSelect}
              {...{
                options: optionsBound,
              }}
            />
          </StyledLabel>
        </Box>
      )}
      <Box mb={[4]}>
        <StyledLabel>
          <Label>Кол-во примеров</Label>
          <InputStyled min={1} name="count" type="number" component="input" />
        </StyledLabel>
      </Box>
    </Fragment>
  );
};

const selector = formValueSelector(HOMEWORK_FORM);

LeftPathform.propTypes = {
  intl: PropTypes.object.isRequired,
  rooms: PropTypes.array.isRequired,
  onChangeNumbers: PropTypes.func.isRequired,
  selectedMode: PropTypes.object.isRequired,
  selectedNumbers: PropTypes.array.isRequired,
  selectedSimulator: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  rooms: state.socket.rooms,
  selectedSimulator: selector(state, 'simulator')
    ? selector(state, 'simulator')
    : {},
  selectedNumbers: selector(state, 'params[numbers]')
    ? selector(state, 'params[numbers]')
    : [],
  selectedMode: selector(state, 'params[mode]')
    ? selector(state, 'params[mode]')
    : {},
});

const mapDispatchToProps = dispatch => ({
  onChangeNumbers(numbersList) {
    dispatch(change(HOMEWORK_FORM, 'params[numbers]', numbersList));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(LeftPathform));
