import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { types, actions, HOMEWORK_FORM } from 'modules/Homework';
import { injectIntl, FormattedMessage } from 'react-intl';

// Components
import { Box } from 'UIKit/Grid';
import Panel from 'components/Panel';
import { H3, FormError, FormSuccess } from 'UIKit/Fonts';

import Loading from 'components/Form/Loading';

import colors from 'UIKit/Colors';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import { RULE_SIMPLE_SUM, numbers } from 'modules/Simulators';
import HomeworkForm from './HomeworkForm';

export const ButtonStyled = styled(Button)`
  background-color: ${colors.brand['500']} !important;
  color: ${colors.white['0']} !important;
`;

/**
 * CreateHomework page.
 * @param {object} values values.
 */
class CreateHomework extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    this.state = {
      query: false,
    };
  }

  /**
   * @param {object} formData formData.
   */
  onSubmit = formData => {
    const { onCreate } = this.props;
    this.setState({ query: true }, () => onCreate(formData));
  };

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const { requests, handleSubmit, intl } = this.props;
    const { query } = this.state;
    const request = requests[types.REQUEST_HOMEWORK_CREATE];
    return (
      <Box w={1} px={4}>
        <Panel>
          <Box px={[1, 8]} py={[0, 4]}>
            <H3>
              {request && query && (
                <Fragment>
                  {request.loading && <Loading />}
                  {request.error && (
                    <FormError>{request.errorMessage}</FormError>
                  )}
                  {request.data && (
                    <FormSuccess>
                      <FormattedMessage
                        id="HOMEWORK_SUCCESS_CREATE_MESSAGE"
                        defaultMessage="HOMEWORK_SUCCESS_CREATE_MESSAGE"
                      />
                    </FormSuccess>
                  )}
                </Fragment>
              )}
              <FormattedMessage
                id="HOMEWORK_TABLE_CREATE_TITLE"
                defaultMessage="HOMEWORK_TABLE_CREATE_TITLE"
              />
            </H3>
          </Box>
          <Box px={[4, 8]} py={[3, 6]}>
            <form onSubmit={handleSubmit(this.onSubmit)}>
              <HomeworkForm
                initialValues={{
                  params: {
                    numbers,
                    steps: 5,
                    range: 1,
                    interval: 1,
                    mode: {
                      value: RULE_SIMPLE_SUM,
                      label: intl.formatMessage({ id: 'RULE_SIMPLE_SUM' }),
                    },
                    sign: { value: 0, label: '+' },
                    over: { value: 1, label: 1 },
                    under: { value: 1, label: 1 },
                    bound: { value: 0, label: 'Без перехода' },
                  },
                  count: 10,
                }}
              />
              <ButtonStyled fullWidth type="submit">
                <FormattedMessage
                  id="HOMEWORK_TABLE_CREATE"
                  defaultMessage="HOMEWORK_TABLE_CREATE"
                />
              </ButtonStyled>
            </form>
          </Box>
        </Panel>
      </Box>
    );
  }
}

CreateHomework.propTypes = {
  intl: PropTypes.object.isRequired,
  requests: PropTypes.object.isRequired,
  onCreate: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  onCreate(formData) {
    dispatch(actions.fetchHomeworkCreate(formData));
  },
});

const mapStateToProps = state => ({
  requests: state.requests,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  reduxForm({
    form: HOMEWORK_FORM,
  })(injectIntl(CreateHomework))
);
