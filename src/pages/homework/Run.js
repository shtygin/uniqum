import React, { useState, useEffect, useRef, useReducer } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';

import { Box, Flex } from 'UIKit/Grid';

import styled from 'styled-components';
import { withRouter } from 'react-router';

import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Column from '../../../vendors/simulators/src/Column';
import FlashCard from '../../../vendors/simulators/src/FlashCard';
import Forsage from '../../../vendors/simulators/src/Forsage';
import SimulatorParams from '../../../vendors/simulators/src/SimulatorParams';
import ControlPanel from '../../../vendors/simulators/src/ControlPanel';
import PersonalPanel from '../../../vendors/simulators/src/PersonalPanel';
import AnswerCounter from '../../../vendors/simulators/src/AnswerCounter';
import Sample from '../../../vendors/simulators/src/Sample';

import { actions, types } from '../../modules/Homework';

// StyledComponents
const DialogTitleStyled = styled(DialogTitle)`
  width: 500px;
  max-width: 500px;
`;

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    minWidth: '230px',
    borderRadius: 0,
    boxShadow: '0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22)',
  },
}));

const RenderRoom = ({ room }) => {
  let component = <Column />;
  switch (room) {
    case 'roomA':
      component = <Forsage />;
      break;
    case 'roomC':
      component = <FlashCard />;
      break;
    default:
      break;
  }

  return component;
};

const DialogBox = ({ open, onClose, history }) => {
  const handleRedirect = () => {
    history.push('/homeworks');
  };

  const handleClose = () => {
    onClose();
  };

  return (
    <Dialog fullScreen={false} open={open} onClose={() => console.log('close')}>
      <DialogTitleStyled>Поздравляем, это задание выполнено!</DialogTitleStyled>
      <DialogContent>
        <table>
          <tbody>
            <tr>
              <td>
                <Button color="primary" type="submit" onClick={handleRedirect}>
                  Перейти к другим заданиям
                </Button>
              </td>
              <td>
                <Button color="primary" type="submit" onClick={handleClose}>
                  продолжить это задание
                </Button>
              </td>
            </tr>
          </tbody>
        </table>
      </DialogContent>
    </Dialog>
  );
};

DialogBox.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

const DialogBoxWithRouter = withRouter(DialogBox);

const SET_CONTROLS = 'SET_CONTROLS';

const initialState = {
  controls: {
    dictation: false,
    start: false,
    stop: false,
    open: false,
    check: false,
    clear: false,
    abacus: false,
    settings: true,
    repeat: false,
    correct: false,
  },
};

function reducer(state, action) {
  switch (action.type) {
    case SET_CONTROLS:
      return {
        ...state,
        [action.meta]: action.payload,
      };
    default:
      return state;
  }
}

const Run = ({
  fetchArgs,
  match,
  homework,
  loadData,
  requests,
  createTask,
}) => {
  const classes = useStyles();
  const [state, dispatch] = useReducer(reducer, initialState);

  const {
    params: { id },
  } = match;

  const homeworkRef = useRef();

  useEffect(() => {
    loadData(id);
  }, []);

  const [openDialog, setOpenDialog] = useState(false);
  const [params, setParams] = useState({});
  const [room, setRoom] = useState('roomA');

  useEffect(() => {
    if (homework) {
      homeworkRef.current = homework;
      if (homework.params) {
        setParams(homework.params);
      }
      if (homework.simulator && homework.simulator.name) {
        setRoom(homework.simulator.name);
      }
    }
  }, [homework]);

  const [args, setArgs] = useState([]);

  const request = requests[types.REQUEST_GET_ARGS];

  useEffect(() => {
    if (request && request.success && request.data.length > 0) {
      setArgs(request.data[0]);
    }
  }, [request]);

  const handleChangeParams = () => {};

  const handleShowParamsBlock = () => ({});

  const handleStart = nextParams => {
    fetchArgs(nextParams);
  };

  const handleClose = () => {
    setOpenDialog(false);
  };

  const handleCheck = () => {
    console.log('handleCheck', state.controls);
    dispatch({
      type: SET_CONTROLS,
      meta: 'controls',
      payload: {
        ...state.controls,
        check: true,
      },
    });
  };

  const handleIncrement = ({ success, failure, args: nextArgs, value }) => {
    const {
      count,
      params: curParams,
      id: homeworkId,
      simulator: { name: trainer },
      student,
    } = homeworkRef.current;
    const sum = nextArgs.reduce((acc, next) => acc + next, 0);
    const data = {
      sum,
      input: value,
      args: nextArgs,
      success: +sum === +value,
      homework: homeworkId,
      ...curParams,
      trainer,
      student,
    };

    createTask(data);
    if (+failure + +success === count) {
      setOpenDialog(true);
    }
  };

  return (
    <>
      <DialogBoxWithRouter open={openDialog} onClose={handleClose} />
      <Flex alignItems="center" justifyContent="space-around">
        <Box
          mb={[6]}
          w={[1, 1 / 2, 1 / 2, 1 / 2]}
          style={{ textAlign: 'center' }}
        >
          {homework && <h3>{homework.comment}</h3>}
          <SimulatorParams
            onShowParamsBlock={handleShowParamsBlock}
            onChangeParams={handleChangeParams}
            params={params}
            room={room}
            open={false}
          />
          <PersonalPanel
            args={args}
            request={request}
            controls={state.controls}
            params={params}
            room={room}
            onStart={handleStart}
            onStop={() => console.log('------- FINISH --------')}
            onCheck={handleCheck}
            onIncrement={handleIncrement}
          >
            <Paper className={classes.paper}>
              {request && request.success && <RenderRoom room={room} />}
              <Sample samples={[args]} afterCheck />
              <ControlPanel />
              <AnswerCounter
                open={false}
                onShowParamsBlock={handleShowParamsBlock}
                visible={{
                  play: true,
                  sound: true,
                  sample: true,
                  settings: false,
                }}
              />
            </Paper>
          </PersonalPanel>
        </Box>
      </Flex>
    </>
  );
};

const mapStateToProps = state => ({
  homework: state.homeworks.homework,
  requests: state.requests,
});

Run.propTypes = {
  homework: PropTypes.object.isRequired,
  requests: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  fetchArgs: PropTypes.func.isRequired,
  loadData: PropTypes.func.isRequired,
  createTask: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  loadData(id) {
    dispatch(actions.fetchHomework(id));
  },
  fetchArgs(params) {
    dispatch(actions.fetchArgs({ params }));
  },
  createTask(data) {
    dispatch(actions.fetchTaskCreate(data));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Run);
