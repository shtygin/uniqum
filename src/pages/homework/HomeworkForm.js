import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import { HOMEWORK_FORM } from 'modules/Homework';

// Components
import { Flex, Box } from 'UIKit/Grid';
import Panel from 'components/Panel';

import colors from 'UIKit/Colors';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import LeftPathform from './LeftPathform';
import RigthPathForm from './RigthPathForm';

export const ButtonStyled = styled(Button)`
  background-color: ${colors.brand['500']} !important;
  color: ${colors.white['0']} !important;
`;

const HomeworkForm = ({ initialValues }) => (
  <Flex flexWrap="wrap" mx={-4}>
    <Box w={[1, 1, 1 / 2, 1 / 2]} px={4} pb={[4, 8]}>
      <Panel>
        <Box px={[4, 8]} py={[3, 6]}>
          <LeftPathform initialValues={initialValues} />
        </Box>
      </Panel>
    </Box>
    <Box w={[1, 1, 1 / 2, 1 / 2]} px={4} pb={[4, 8]}>
      <Panel>
        <Box px={[4, 8]} py={[3, 6]}>
          <RigthPathForm />
        </Box>
      </Panel>
    </Box>
  </Flex>
);

HomeworkForm.propTypes = {
  initialValues: PropTypes.object.isRequired,
};

export default reduxForm({
  form: HOMEWORK_FORM,
})(HomeworkForm);
