import React from 'react';
import { Flex, Box } from 'UIKit/Grid';
import { H1, H3 } from 'UIKit/Fonts';
import { FormattedMessage } from 'react-intl';

const NotFound = () => (
  <Flex w={1} pl={[4, 8, 16]} pr={[4, 22, 16]} py={[10, 5, 9]}>
    <Box>
      <H1>
        <FormattedMessage id="notFound.title" defaultMessage="Home page" />
      </H1>
      <H3>
        <FormattedMessage id="notFound.subtitle" defaultMessage="Home page" />
      </H3>
    </Box>
  </Flex>
);

export default NotFound;
