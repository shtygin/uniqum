import React, { useState, useEffect, useReducer, useRef } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Dictation from 'components/Dashboard/Dictation';
import { actions, types } from 'modules/ArifmetikBook';
import { sumAbacus } from 'modules/Simulators';
import RenderDictation from 'styledComponents/RenderDictation';
import SimulatorParams from '../../../vendors/simulators/src/SimulatorParams';

const SET_ROOM = 'SET_ROOM';
const SET_PARAMS = 'SET_PARAMS';
const SET_CONTROLS = 'SET_CONTROLS';
const SET_ABACUS_STATE = 'SET_ABACUS_STATE';
const SET_DICTATION_STATE = 'SET_DICTATION_STATE';

const initialState = {
  params: {
    section: 5,
    steps: 3,
    interval: 1,
    mode: 1,
    sign: 0,
    bound: 0,
    under: 1,
    over: 1,
    numbers: ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
  },
  controls: {
    start: false,
    stop: false,
    open: false,
    check: false,
    clear: false,
    abacus: false,
    settings: true,
    repeat: false,
    correct: false,
  },
  visible: {
    answerCounter: {
      sound: false,
      settings: true,
      sample: true,
      play: true,
      params: true,
      showAnswer: true,
      abacus: true,
    },
  },
  dictationState: {
    dictation: false,
    amount: 3,
    start: false,
    progress: false,
    stop: false,
    period: 5,
    samples: [],
  },
  abacusState: {
    a: [0, 0, 0, 0, 0],
    b: [0, 0, 0, 0, 0],
    c: [0, 0, 0, 0, 0],
    d: [0, 0, 0, 0, 0],
    e: [0, 0, 0, 0, 0],
  },
};

function reducer(state, action) {
  switch (action.type) {
    case SET_ROOM:
    case SET_PARAMS:
    case SET_CONTROLS:
    case SET_ABACUS_STATE:
    case SET_DICTATION_STATE: {
      return {
        ...state,
        [action.meta]: action.payload,
      };
    }
    default:
      return state;
  }
}

const WrapTerminal = ({
  state,
  parentRequest,
  childRequest,
  num,
  fetchArgsOnce,
}) => {
  const { params, controls, visible, room, dictationState } = state;
  const { dictation, amount } = dictationState;
  const [terminalState, dispatch] = useReducer(reducer, {
    ...initialState,
    params,
    controls,
    room,
    visible,
    dictationState,
  });

  /*
   * Ref
   */
  const controlsRef = useRef(controls);
  const argsRef = useRef([]);

  /*
   * End Ref
   */

  useEffect(() => {
    dispatch({
      type: SET_ROOM,
      meta: 'room',
      payload: room,
    });
  }, [room]);

  useEffect(() => {
    dispatch({
      type: SET_PARAMS,
      meta: 'params',
      payload: params,
    });
  }, [params]);

  useEffect(() => {
    controlsRef.current = controls;
    dispatch({
      type: SET_CONTROLS,
      meta: 'controls',
      payload: controls,
    });
  }, [controls]);

  useEffect(() => {
    dispatch({
      type: SET_DICTATION_STATE,
      meta: 'dictationState',
      payload: dictationState,
    });
  }, [dictationState]);

  const [request, setRequest] = useState({});

  useEffect(() => {
    setRequest(childRequest);
    if (childRequest && childRequest.data && childRequest.data.length > 0) {
      argsRef.current = childRequest.data;
    }
  }, [childRequest]);

  useEffect(() => {
    setRequest(parentRequest);
    if (parentRequest && parentRequest.data && parentRequest.data.length > 0) {
      const nextArgs = parentRequest.data.slice(
        num * amount,
        num * amount + amount
      );
      argsRef.current = nextArgs;
    }
  }, [parentRequest]);

  const [args, setArgs] = useState([]);

  useEffect(() => {
    if (request && request.success) {
      setArgs(request.data[num]);
    }
  }, [request, dictation]);

  const handleGetNextArgs = index => {
    setArgs(argsRef.current[index]);
  };

  function handleGetArgs(nextParams, nextDictation, nextAmount) {
    let count = 1;
    if (nextDictation) {
      count = nextAmount;
    }
    fetchArgsOnce(nextParams, count);
  }

  const handleStop = () => {
    dispatch({
      type: SET_CONTROLS,
      meta: 'controls',
      payload: {
        ...controlsRef.current,
        stop: true,
        start: false,
        check: false,
      },
    });
  };

  const handleCheck = () => {
    dispatch({
      type: SET_CONTROLS,
      meta: 'controls',
      payload: {
        ...terminalState.controls,
        check: true,
        stop: false,
        start: false,
      },
    });
  };

  const handleIncrement = values => {
    console.log('Increment ', values);
  };

  const handleChangeParams = nextParams => {
    dispatch({
      type: SET_PARAMS,
      meta: 'params',
      payload: nextParams,
    });
  };

  const handleShowParamsBlock = () => {
    dispatch({
      type: SET_CONTROLS,
      meta: 'controls',
      payload: {
        ...terminalState.controls,
        open: !terminalState.controls.open,
      },
    });
  };

  const handleAbacusChange = (rodData, rodLable, nextAbacusState) => {
    dispatch({
      type: SET_ABACUS_STATE,
      meta: 'abacusState',
      payload: nextAbacusState,
    });
    dispatch({
      type: SET_CONTROLS,
      meta: 'controls',
      payload: {
        ...terminalState.controls,
        value: sumAbacus(nextAbacusState),
      },
    });
  };

  const handleToggleSetting = (event, name) => {
    dispatch({
      type: SET_CONTROLS,
      meta: 'controls',
      payload: {
        ...terminalState.controls,
        [name]: event.target.checked,
      },
    });
  };

  const handleChangeDictationSetting = time => {
    dispatch({
      type: SET_DICTATION_STATE,
      meta: 'dictationState',
      payload: { ...dictationState, period: time },
    });
  };

  return (
    <>
      <SimulatorParams
        onChangeParams={handleChangeParams}
        params={terminalState.params}
        room={room}
        fullSize
        onShowParamsBlock={handleShowParamsBlock}
        open={terminalState.controls.open}
      />
      <Dictation
        state={terminalState}
        args={args}
        onAbacusChange={handleAbacusChange}
        onStop={handleStop}
        onCheck={handleCheck}
        onIncrement={handleIncrement}
        room={room}
        onToggleSetting={handleToggleSetting}
        request={request}
        onShowParamsBlock={handleShowParamsBlock}
        onChangeDictationSetting={handleChangeDictationSetting}
        fetchArgs={handleGetArgs}
        getNextArgs={handleGetNextArgs}
        render={props => <RenderDictation {...props} />}
      />
    </>
  );
};

WrapTerminal.propTypes = {
  num: PropTypes.number.isRequired,
  state: PropTypes.object.isRequired,
  fetchArgsOnce: PropTypes.func.isRequired,
  parentRequest: PropTypes.any.isRequired,
  childRequest: PropTypes.any.isRequired,
};

const mapStateToProps = (state, props) => {
  const { num } = props;
  const request = state.requests[`${types.REQUEST_GET_ARGS}_${num}`];
  return {
    childRequest: request,
    // data: state.arifmetikBook.data,
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  fetchArgsOnce(params, count) {
    dispatch(actions.fetchArgsOnce(props.num, count, { params }));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(WrapTerminal);
