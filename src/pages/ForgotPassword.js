import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import { types, actions } from 'modules/Guest';

// Components
import { H1Panel, H2Panel, FormError } from 'UIKit/Fonts';
import { Box } from 'UIKit/Grid';
import Loading from 'components/Form/Loading';
import SuccessMessage from 'components/SuccessMessage';
import ForgotPasswordForm from 'pages/forgotpassword/ForgotPasswordForm';

/**
 * ForgotPassword page.
 * @param {object} values values.
 */
class ForgotPassword extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  /**
   * @param {object} formData данные формы.
   */
  handleSubmit(formData) {
    const { forgotPassword } = this.props;
    forgotPassword(formData);
  }

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const { requests } = this.props;
    const request = requests[types.REQUEST_FORGOT_PASSWORD];

    return (
      <Fragment>
        {request && (
          <div>
            {request.loading && <Loading />}
            {request.error && (
              <FormError>
                <FormattedMessage id={request.errorMessage} defaultMessage="" />
              </FormError>
            )}
          </div>
        )}
        {request && request.success ? (
          <SuccessMessage
            message={
              <FormattedMessage // eslint-disable-line react/jsx-wrap-multilines
                id="forgotPassword.success_message"
                defaultMessage=""
              />
            }
          />
        ) : (
          <Fragment>
            <Box mb={8}>
              <Box mb={1}>
                <H1Panel>
                  <FormattedMessage
                    id="forgotPassword.title"
                    defaultMessage=""
                  />
                </H1Panel>
              </Box>
              <H2Panel>
                <FormattedMessage
                  id="forgotPassword.subtitle"
                  defaultMessage=""
                />
              </H2Panel>
            </Box>
            <ForgotPasswordForm onSubmit={this.handleSubmit} />
          </Fragment>
        )}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  requests: state.requests,
});

const mapDispatchToProps = dispatch => ({
  forgotPassword(formData) {
    dispatch(actions.fetchForgotPassword(formData));
  },
});

ForgotPassword.propTypes = {
  requests: PropTypes.object.isRequired,
  forgotPassword: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);
