import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

const SuccessMessage = ({ message }) => <div>{message}</div>;

SuccessMessage.propTypes = {
  message: PropTypes.object,
};

SuccessMessage.defaultProps = {
  message: (
    <FormattedMessage id="common.success_message" defaultMessage="Success" />
  ),
};

export default SuccessMessage;
