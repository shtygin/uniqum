import styled, { css } from 'styled-components';
import colors from 'UIKit/Colors';
import mq from 'UIKit/Mq';

const Panel = styled.div`
  position: relative;
  overflow: hidden;
  height: 100%;
  border-radius: 4px;
  background-color: ${colors.white['0']};
  box-shadow: 0 4px 11px ${colors.shadow['150']};
  ${({ table }) =>
    table &&
    css`
      max-width: 100vw;
      overflow-x: auto;
      ${mq.desktop1366`
      	max-width: 100vw;
      `};
      ${mq.desktop1200`
      	max-width: 100vw;
      `};
      ${mq.tabletHorizontal`
      	max-width: 100vw;
      `};
      ${mq.tablet`
      	max-width: 100vw;
      `};
    `};
  ${({ green }) =>
    green &&
    css`
      background-image: linear-gradient(-225deg, #1de9b6 0%, #1dc4e9 100%);
      box-shadow: 0 10px 20px 0 rgba(0, 0, 0, 0.2);
    `};
`;

export default Panel;
