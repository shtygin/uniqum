/**
 * App Header
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import MenuIcon from '@material-ui/icons/Menu';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';

// Styles
import styled from 'styled-components';
import colors from 'UIKit/Colors';
// import mq from 'UIKit/Mq';
import { Flex, SimpleList } from 'UIKit/Grid';
import { StyledLink } from 'UIKit/Fonts';

// components
// import LanguageProvider from './LanguageProvider';

// StyledComponents
const AppBarStyled = styled(AppBar)`
  position: relative !important;
  z-index: 1 !important;
  box-shadow: 0px 1px 10px -1px rgba(0, 0, 0, 0.2) !important;
`;
const ToolbarStyled = styled(Toolbar)`
  background-color: ${colors.white['0']};
  color: ${colors.gray['900']};
`;
const SimpleListFlex = styled(SimpleList)`
  display: flex;
  align-items: center;
`;

const UserName = styled.div`
  color: black;
  font-weight: 700;
`;
// const StyledLanguageProvider = styled(LanguageProvider)`
//   ${mq.tablet`
//     display: none;
//   `};
// `;

/**
 * Header
 * @param {bullean} open open.
 */
class Header extends Component {
  onSidebarUpdate = () => {
    const { onSetSidebarOpen, navCollapsed } = this.props;
    onSetSidebarOpen(!navCollapsed);
  };

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const { user } = this.props;
    return (
      <AppBarStyled position="static">
        <ToolbarStyled>
          <Flex alignItems="center" justifyContent="space-between" w="100%">
            <SimpleList>
              <li>
                <IconButton
                  color="inherit"
                  mini="true"
                  aria-label="Menu"
                  className="humburger"
                  onClick={this.onSidebarUpdate}
                >
                  <MenuIcon />
                </IconButton>
              </li>
            </SimpleList>
            <SimpleListFlex>
              {user && user.name && user.surname && (
                <UserName>
                  {user.surname} {user.name}
                </UserName>
              )}
              {/* <LanguageProvider /> */}
            </SimpleListFlex>
          </Flex>
        </ToolbarStyled>
      </AppBarStyled>
    );
  }
}

Header.propTypes = {
  onSetSidebarOpen: PropTypes.func.isRequired,
  navCollapsed: PropTypes.any.isRequired,
  user: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  user: state.user,
});

export default connect(mapStateToProps)(Header);

/**
 * HeaderPublic
 * @returns {*}
 * @param {*} props props.
 */
export const HeaderPublic = () => {
  const location = window.location.pathname;
  return (
    <Flex
      alignItems="center"
      justifyContent={['space-between', 'flex-end']}
      w="100%"
      pt={[4, 6]}
      px={[4, 10]}
    >
      <SimpleListFlex>
        <li>
          {location === '/signin' ? (
            <StyledLink to="/signup">
              {/* <FormattedMessage id="nav.signup" defaultMessage="Home page" /> */}
            </StyledLink>
          ) : (
            <StyledLink to="/signin">
              <FormattedMessage id="nav.signin" defaultMessage="Home page" />
            </StyledLink>
          )}
        </li>
        {/* <StyledLanguageProvider /> */}
      </SimpleListFlex>
    </Flex>
  );
};
