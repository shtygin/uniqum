import cookie from 'react-cookies';
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  setLocale,
  setLocaleMessages,
  fetchChangeLocale,
  listAvailableLocale as languages,
} from 'modules/Locale';
import Tooltip from '@material-ui/core/Tooltip';
import { DropdownToggle, DropdownMenu, Dropdown } from 'reactstrap';

// Styles
import styled, { css } from 'styled-components';
import colors from 'UIKit/Colors';
import { SimpleList } from 'UIKit/Grid';

// StyledComponents
const DropdownStyled = styled(Dropdown)`
  position: relative;
  margin-left: 15px;
`;
const DropdownToggleStyled = styled(DropdownToggle)`
  position: relative;
  padding: 9px 30px 9px 15px;
  border: 0;
  border-radius: 4px;
  background-color: transparent;
  font-size: 0;
  transition: 0.3s;
  &:hover {
    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.36);
  }
  &:before {
    content: '';
    position: absolute;
    top: 50%;
    right: 9px;
    width: 5px;
    height: 5px;
    margin-top: -4px;
    border-bottom: 2px solid ${colors.gray['800']};
    border-left: 2px solid ${colors.gray['800']};
    transform: rotate(-45deg);
  }
`;
const DropdownMenuStyled = styled(DropdownMenu)`
  position: absolute;
  visibility: hidden;
  opacity: 0;
  right: 0 !important;
  left: auto !important;
  top: 100% !important;
  width: 200px;
  margin-top: 18px;
  border-radius: 2px;
  background-color: ${colors.white['0']};
  box-shadow: rgba(69, 65, 78, 0.08) 0px 1px 15px 1px;
  transform: translateY(50px);
  transition: opacity 0.3s, transform 0.3s;
  ${({ open }) =>
    open &&
    css`
      visibility: visible;
      opacity: 1;
      transform: translateY(0) !important;
    `};
`;
const DropdownItem = styled.li`
  position: relative;
  display: flex;
  align-items: center;
  padding: 10px 19px;
  cursor: pointer;
  transition: background-color 0.3s;
  &:hover {
    background-color: ${colors.brand['200']};
    transition: none;
  }
  &:first-child {
    border-radius: 2px 2px 0 0;
  }
  &:last-child {
    border-radius: 0 0 2px 2px;
  }
`;
const LangName = styled.span`
  &:not(:first-child) {
    margin-left: 10px;
  }
`;

/**
 * Language Select Dropdown
 */
class LanguageProvider extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    this.state = {
      langDropdownOpen: false,
      selectedLocale: props.user.lang,
    };

    this.onChangeLanguage = this.onChangeLanguage.bind(this);
  }

  /**
   * Life cycle.
   * @param {object} nextProps props.
   */
  componentWillReceiveProps(nextProps) {
    const {
      user: { lang: nextLang },
    } = nextProps;
    this.setState({ selectedLocale: nextLang });
  }

  /**
   * Change language
   * @param {string} language - локаль
   */
  onChangeLanguage(language) {
    const { setLanguage, authenticated, fetchLanguage } = this.props;
    this.setState({ langDropdownOpen: false });
    setLanguage(language.locale);
    if (authenticated) {
      fetchLanguage(language.locale);
    }
    cookie.save('language', language.locale);
  }

  // function to toggle dropdown menu
  toggle = () => {
    const { langDropdownOpen } = this.state;
    this.setState({
      langDropdownOpen: !langDropdownOpen,
    });
  };

  /* eslint-disable import/no-dynamic-require, global-require */
  /**
   * Render
   * @returns {object}.
   */
  render() {
    const { langDropdownOpen, selectedLocale } = this.state;
    return (
      <DropdownStyled isOpen={langDropdownOpen} toggle={this.toggle}>
        <DropdownToggleStyled caret>
          <Tooltip title="Languages" placement="bottom">
            <img
              src={require(`assets/flag-icons/${selectedLocale}.png`)}
              className="mr-10"
              width="25"
              height="16"
              alt="lang-icon"
            />
          </Tooltip>
        </DropdownToggleStyled>
        <DropdownMenuStyled open={langDropdownOpen}>
          <SimpleList>
            {languages.map(language => (
              <DropdownItem
                key={language.locale}
                onClick={() => this.onChangeLanguage(language)}
              >
                <img
                  src={require(`assets/flag-icons/${language.icon}.png`)} // eslint-disable-line import/no-dynamic-require, global-require
                  width="25"
                  height="16"
                  alt="lang-icon"
                />
                <LangName>{language.name}</LangName>
              </DropdownItem>
            ))}
          </SimpleList>
        </DropdownMenuStyled>
      </DropdownStyled>
    );
  }
}

LanguageProvider.propTypes = {
  setLanguage: PropTypes.func.isRequired,
  fetchLanguage: PropTypes.func.isRequired,
  authenticated: PropTypes.bool.isRequired,
  user: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
};

const mapStateToProps = state => ({
  user: state.user,
  authenticated: state.user.token !== '',
});

const mapDispatchToProps = dispatch => ({
  setLanguage(locale) {
    dispatch(setLocale(locale));
    dispatch(setLocaleMessages(locale));
  },
  fetchLanguage(locale) {
    dispatch(fetchChangeLocale({ lang: locale }));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LanguageProvider);
