import { screens } from 'UIKit/Mq';
import colors from 'UIKit/Colors';
import range from 'lodash.range';

const UNIT = 4;

export const getSize = size => (typeof size === 'string' ? size : size * UNIT);
export const px = x => (typeof x === 'string' ? x : `${x}px`);

const theme = {
  breakpoints: Object.keys(screens).map(key => screens[key]),
  space: range(0, 60).map(x => x * UNIT),
  colors,
  getSpace: size => {
    if (Array.isArray(size)) {
      return size.map(x => getSize(x));
    }

    return getSize(size);
  },
  getSpacePx: size => {
    if (Array.isArray(size)) {
      return size
        .map(x => getSize(x))
        .map(x => px(x))
        .join(' ');
    }

    return px(getSize(size));
  },
};

export default theme;
