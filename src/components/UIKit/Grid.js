import styled from 'styled-components';
import {
  style,
  position,
  space,
  display,
  width,
  height,
  maxWidth,
  minWidth,
  // ratio,
  flex,
  alignSelf,
  flexDirection,
  flexWrap,
  justifyContent,
  alignItems,
  textAlign,
  lineHeight,
  fontWeight,
  letterSpacing,
  color,
  zIndex,
  backgroundImage,
  backgroundSize,
  backgroundPosition,
  top,
  right,
  bottom,
  left,
  order,
  maxHeight,
  minHeight,
  borders,
  fontSize,
  borderRadius,
} from 'styled-system';

const overflow = style({
  prop: 'overflow',
  cssProperty: 'overflow',
});

const opacity = style({
  prop: 'opacity',
  cssProperty: 'opacity',
});

const verticalAlign = style({
  prop: 'verticalAlign',
  cssProperty: 'verticalAlign',
});

export const Box = styled.div`
  ${space};
  ${width};
  ${height};
  ${minWidth};
  ${maxWidth};
  ${minHeight};
  ${maxHeight};
  ${overflow};
  ${opacity};
  ${alignSelf};
  ${display};
  ${textAlign};
  ${position};
  ${color};
  ${flex};
  ${order};
  ${top};
  ${right};
  ${bottom};
  ${left};
  ${zIndex};
  ${backgroundImage};
  ${backgroundSize};
  ${backgroundPosition};
  ${verticalAlign};
  ${borders};
  ${borderRadius};
`;

export const Flex = styled.div`
  display: flex;
  ${display};
  ${space};
  ${position};
  ${order} ${overflow};
  ${width};
  ${height};
  ${minWidth};
  ${maxWidth};
  ${maxHeight};
  ${flex};
  ${flexDirection};
  ${flexWrap};
  ${justifyContent};
  ${alignItems};
  ${alignSelf};
  ${display};
  ${color};
  ${top};
  ${right};
  ${bottom};
  ${left};
  ${zIndex};
  ${borders};
`;

export const Text = styled.span`
  ${color};
  ${textAlign};
  ${lineHeight};
  ${fontSize};
  ${fontWeight};
  ${letterSpacing};
`;

export const SimpleList = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0;
`;
