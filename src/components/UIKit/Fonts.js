import { Link } from 'react-router-dom';
import styled from 'styled-components';
import mq from 'UIKit/Mq';
import { color } from 'styled-system';
import colors from 'UIKit/Colors';

export const H1Panel = styled.h1`
  font-size: 1.25rem;
  line-height: 1.2;
  font-weight: ${({ weight }) => weight || 700};
  margin: 0;
  ${color};
`;

export const H1 = styled.h1`
  margin: 0;
  color: #3d3d3d;
  font-size: 32px;
  font-weight: 400;
  ${color};
`;

export const H2Panel = styled.h2`
  font-size: 0.875rem;
  font-weight: ${({ weight }) => weight || 400};
  line-height: ${({ line }) => line || 1.315};
  margin: 0;
  ${color};
`;

export const FormError = styled.div`
  margin-bottom: 24px;
  padding: 4px 16px;
  border-radius: 2px;
  background-color: ${colors.red['300']};
  color: ${colors.white['0']};
  font-size: 13px;
`;

export const FormSuccess = styled.div`
  margin-bottom: 24px;
  padding: 4px 16px;
  border-radius: 2px;
  background-color: ${colors.green['300']};
  color: ${colors.white['0']};
  font-size: 13px;
`;

export const H3 = styled.h3`
  margin: 0;
  font-size: 2.1rem;
  line-height: 1.25;
  font-weight: ${({ weight }) => weight || 700};
  ${color};

  ${mq.desktop1280`font-size: 3.2rem`};
`;

export const H4 = styled.h4`
  margin: 0;
  font-size: 2rem;
  line-height: 1.25;
  font-weight: ${({ weight }) => weight || 700};
  ${color};

  ${mq.desktop1280`font-size: 3.2rem`};
`;

export const H6 = styled.h6`
  margin: 0;
  color: #3d3d3d;
  font-size: 16px;
  font-weight: 400;
  ${color};
`;

export const WalletSum = styled.span`
  color: #1f1f1f;
  font-size: 24px;
  font-weight: 400;
  ${color};
  &:not(:last-child) {
    margin-right: 0.2em;
  }
`;

export const WalletVal = styled.span`
  color: #878788;
  font-size: 24px;
  font-weight: 400;
  text-transform: uppercase;
  ${color};
`;

export const Badge = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 36px;
  margin: 0;
  padding: 2px 15px;
  background-color: #36af47;
  text-transform: uppercase;
  color: #fff;
  font-size: 14px;
  font-weight: 500;
  border-radius: 3px;
  ${color};
`;

export const StyledLink = styled(Link)`
  color: #a3a3a3;
  text-decoration: none;
  transition: color 0.3s ease-in;
  ${color};
  &:hover {
    color: ${colors.brand['400']};
    transition: none;
  }
`;
