import CreateColorProxy from './lib/CreateColorProxy';

// #ffcc14 -желтый
const colors = {
  brand: CreateColorProxy({
    0: '#ebedf0',
    50: '#f4f7fa',
    100: 'rgba(0, 208, 189, 0.1)',
    200: 'rgba(0, 208, 189, 0.2)',
    400: '#35bdb2', // изменил цвет фона лого по градиенту до (темной синий)
    500: '#fd9639',
    600: '#39281e', // изменил цвет фона лого по градиенту от (темно синий)
    700: '#fd9639', // цвет иконок меню (желтый), кнопка на авторизации
    800: '#fff',
  }),
  blue: CreateColorProxy(
    {
      100: '#f8f9fa',
      300: '#374fd6',
      400: '#3547ad',
      500: '#2439af',
    },
    'blue'
  ),
  white: CreateColorProxy(
    {
      0: '#ffffff',
      100: 'rgba(255, 255, 255, 0.2)',
      300: 'rgba(255, 255, 255, 0.3)',
      400: 'rgba(255, 255, 255, 0.4)',
      600: 'rgba(255, 255, 255, 0.6)',
      750: 'rgba(255, 255, 255, 0.75)',
      900: 'rgba(255, 255, 255, 0.9)',
    },
    'white',
    0
  ),
  gray: CreateColorProxy(
    {
      // 0: '#fff',
      400: '#d8d8d8',
      500: '#b8b8b8',
      // 600: '#b3b3b3',
      // 750: '#999',
      800: '#838383',
      900: '#3d3d3d',
      1000: '#000',
    },
    'gray'
  ),
  shadow: CreateColorProxy({
    0: 'rgba(0, 0, 0, 0.06)',
    100: 'rgba(0, 0, 0, 0.08)',
    120: 'rgba(0, 0, 0, 0.12)',
    140: 'rgba(0, 0, 0, 0.14)',
    150: 'rgba(25, 42, 70, 0.08)',
    200: 'rgba(0, 0, 0, 0.2)',
  }),
  // yellow: CreateColorProxy(
  //   {
  //     300: '#fff9a3',
  //   },
  //   'yellow'
  // ),
  red: CreateColorProxy(
    {
      300: '#f55145',
    },
    'red'
  ),
  green: CreateColorProxy(
    {
      300: 'green',
    },
    'green'
  ),
};

export default colors;
