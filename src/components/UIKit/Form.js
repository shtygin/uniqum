import styled, { css } from 'styled-components';
import colors from 'UIKit/Colors';
import { Field } from 'redux-form';
import Button from '@material-ui/core/Button';

import { Box } from 'UIKit/Grid';

export const FormGroup = styled.div`
  position: relative;
  &:not(:last-child) {
    margin-bottom: 24px;
  }
`;

export const InputIcon = styled.div`
  position: absolute;
  top: 9px;
  right: 24px;
  svg {
    fill: ${colors.gray['900']};
  }
`;

export const InputDateGroup = styled.div`
  display: flex;
  border: 1px solid #ddd;
  border-radius: 2px;
  input {
    height: 36px;
    border: 0;
  }
`;

export const InputDateIcon = styled.div`
  opacity: 0.4;
  display: flex;
  align-items: center;
  padding: 0 16px;
  color: ${colors.brand['500']};
`;

export const InputStyled = styled(Field)`
  position: relative;
  width: 100% !important;
  max-width: 100%;
  height: 36px;
  padding: 0.625rem;
  padding-right: 35px;
  border: 1px solid #ddd;
  border-radius: 2px;
  color: #464d69;
  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  overflow: hidden;
  text-overflow: ellipsis;
  &:focus {
    border-color: ${colors.brand['500']};
    outline: 0;
  }
  ${({ icon }) =>
    icon &&
    css`
      padding-left: 48px;
    `};
`;

export const NumStyled = styled(InputStyled)`
  width: 96px !important;
  padding-right: 16px;
  ${({ long }) =>
    long &&
    css`
      width: 100% !important;
      max-width: 160px !important;
    `};
`;

export const TextareaStyled = styled(InputStyled)`
  min-height: 112px;
`;

export const IconForInput = styled(Box)`
  position: absolute;
  z-index: 1;
  bottom: 9px;
  left: 16px;
`;

export const Toggle = styled.label`
  input {
    position: absolute;
    left: -9999px;
    &:focus,
    &:active {
      outline: 0;
    }
    &:checked {
      ~ span {
        background-image: linear-gradient(-225deg, #1de9b6 0%, #1dc4e9 100%);
        &:after {
          transform: translateX(0);
        }
      }
    }
  }
  span {
    position: relative;
    cursor: pointer;
    display: block;
    width: 38px;
    height: 20px;
    border-radius: 83px;
    background: #e3e3e3;
    &:after {
      content: '';
      position: absolute;
      display: block;
      top: 2px;
      left: 50%;
      width: 16px;
      height: 16px;
      border-radius: 100%;
      background-color: #fff;
      transform: translateX(-100%);
      transition: transform 0.3s ease-in-out;
    }
  }
`;

export const RadioBtn = styled.label`
  user-select: none;
  input {
    position: absolute;
    left: -9999px;
    &:focus,
    &:active {
      outline: 0;
    }
    &:checked {
      ~ div {
        &:before {
          opacity: 1;
        }
        &:after {
          border-color: transparent;
          transform: scale(0.4);
        }
      }
    }
  }
  div {
    position: relative;
    cursor: pointer;
    display: block;
    padding-left: 24px;
    &:before {
      content: '';
      position: absolute;
      display: block;
      opacity: 0;
      top: 50%;
      left: 0;
      width: 16px;
      height: 16px;
      margin-top: -8px;
      border-radius: 100%;
      border: 1px solid transparent;
      background-image: linear-gradient(-225deg, #1de9b6 0%, #1dc4e9 100%);
      transition: all 0.15s ease-in-out;
    }
    &:after {
      content: '';
      position: absolute;
      display: block;
      opacity: 1;
      top: 50%;
      left: 0;
      width: 16px;
      height: 16px;
      margin-top: -8px;
      border: 1px solid #e1e1e1;
      border-radius: 100%;
      background-color: #fff;
      transition: all 0.15s ease-in-out;
    }
  }
`;

export const StyledLabel = styled.label`
  position: relative;
  display: block;
`;
export const Label = styled.div`
  font-size: 13px;
  color: #a3a3a3;
  &:not(:last-child) {
    margin-bottom: 4px;
  }
`;

export const LabelIcon = styled.span`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  vertical-align: middle;
  cursor: pointer;
  opacity: 0.5;
  width: 16px;
  height: 16px;
  border-radius: 3px;
  border: 1px solid #ff9e0f;
  color: #ff9e0f;
  font-size: 10px;
  line-height: 1;
  text-align: center;
  transition: opacity 0.15s ease-in-out;
  &:before {
    content: '';
    position: absolute;
  }
  &:not(:first-child) {
    margin-left: 8px;
  }
  &:hover {
    opacity: 1;
  }
`;

export const Copied = styled.div`
  font-size: 0.875rem;
  color: green;
  &:not(:first-child) {
    margin-top: 4px;
  }
`;

export const FieldGroup = styled.div`
  display: flex;
  border: 1px solid #ddd;
  border-radius: 2px;
`;
export const FieldLabel = styled.div`
  display: flex;
  align-items: center;
  padding: 0 16px;
  border-radius: 2px;
  white-space: nowrap;
  font-size: 16px;
  color: #a3a3a3;
`;

export const FieldStyled = styled(Field)`
  width: 100%;
  height: 36px;
  padding: 0.625rem;
  border: 0;
  border-radius: 0;
  color: #464d69;
  text-align: right;
  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  &:not(:last-child) {
    border-right: 1px solid #ddd;
  }
`;

export const SelectStyled = styled(Field)`
  width: 100%;
  height: 36px;
  padding: 0.625rem;
  border: 0;
  text-align: right;
  background-color: #fff;
  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  &:not(:last-child) {
    border-top-right-radius: 0px;
    border-bottom-right-radius: 0px;
  }
  &:not(:first-child) {
    border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;
  }
`;

export const StyledButton = styled(Button)`
  //min-width: 192px !important;
  background-color: ${colors.brand['500']} !important;
  color: #fff !important;
  background-image: linear-gradient(
    -45deg,
    ${colors.brand['700']} 0%,
    ${colors.brand['700']} 100%
  );
  box-shadow: 0 10px 20px 0 rgba(0, 0, 0, 0.15) !important;
  border-radius: 2px !important;
  transition: all 0.3s ease-in;
  &:disabled {
    opacity: 0.5;
    box-shadow: none;
  }
  &:hover {
    box-shadow: 0 10px 20px 0 rgba(0, 0, 0, 0.2) !important;
    transition: none;
  }
  ${({ lite }) =>
    lite &&
    css`
      background: #fff !important;
      color: #000 !important;
    `};
`;

export const StyledButtonIcon = styled(Button)`
  padding: 8px !important;
  min-width: 36px !important;
  border-radius: 100% !important;
  font-size: 20px;
  ${({ del }) =>
    del &&
    css`
      color: red !important;
    `};
  ${({ save }) =>
    save &&
    css`
      color: green !important;
    `};
  &:disabled {
    opacity: 0.5;
  }
`;

export const StyledLink = styled(Button)`
  position: relative;
  min-height: 0 !important;
  height: auto !important;
  padding: 0 !important;
  text-transform: none !important;
  color: ${colors.brand['500']} !important;
  &:before {
    content: '';
    position: absolute;
    opacity: 0.65;
    left: 0;
    right: 0;
    bottom: 0;
    border-bottom: 1px dashed ${colors.brand['500']};
  }
`;
