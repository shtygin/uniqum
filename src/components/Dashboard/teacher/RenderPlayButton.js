import React from 'react';

import Button from '@material-ui/core/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlayCircle, faPauseCircle } from '@fortawesome/free-solid-svg-icons';

const RenderStartButton = ({ play, onStart, onStop }) => {
  const handleStart = () => {
    onStart();
  };

  const handleStop = () => {
    onStop();
  };

  let component = (
    <Button
      variant="contained"
      onClick={handleStart}
      startIcon={<FontAwesomeIcon icon={faPlayCircle} />}
    >
      Старт
    </Button>
  );

  if (play) {
    component = (
      <Button
        variant="contained"
        onClick={handleStop}
        startIcon={<FontAwesomeIcon icon={faPauseCircle} />}
      >
        Стоп
      </Button>
    );
  }

  return component;
};

export default RenderStartButton;
