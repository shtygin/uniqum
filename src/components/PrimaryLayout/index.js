import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

// Styles
import styled, { css } from 'styled-components';
import { Flex, Box } from 'UIKit/Grid';
import colors from 'UIKit/Colors';
import mq from 'UIKit/Mq';
import Panel from 'components/Panel';

// Components
import Header, { HeaderPublic } from 'components/Header';
import Sidebar from 'components/Sidebar';
import Container from 'components/Layout/Container';

// StyledComponents
const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  background: url('/assets/img/wrapper_bg.jpg') repeat;
`;
const WrapperPublic = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  overflow: overlay;
  background: url('/assets/img/wrapper_bg.jpg') repeat;
`;
const SidebarCloser = styled.div`
  position: fixed;
  display: none;
  z-index: 2;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  opacity: 1;
  background-color: rgba(0, 0, 0, 0.3);
  transition: opacity 0.3s;
  transform: translateX(0);
  ${mq.tabletHorizontal`
    display: block;
    `};
  ${({ navCollapsed }) =>
    navCollapsed &&
    css`
      opacity: 0;
      transform: translateX(-100%);
    `};
`;

const Logo = styled(Link)`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  min-height: 300px;
  color: ${colors.white['0']};
  background: url('/assets/img/bg_logo.png') center bottom no-repeat;
`;

/**
 * PrimaryLayout
 * @param {bullean} open open.
 */
export default class PrimaryLayout extends Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    this.state = {
      windowWidth: window.screen.width,
      navCollapsed: window.screen.width <= 1199,
    };
  }

  /**
   * Update component
   */
  componentWillMount() {
    this.updateDimensions();
  }

  /**
   * Update component
   */
  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions);
  }

  /**
   * Update component
   */
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
  }

  onSetSidebarOpen = open => {
    this.setState({ navCollapsed: open });
  };

  updateDimensions = () => {
    this.setState({
      windowWidth: window.screen.width,
      navCollapsed: window.screen.width <= 1199,
    });
  };

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const { windowWidth, navCollapsed } = this.state;
    const { children } = this.props;
    return (
      <Wrapper>
        <Flex flexWrap="nowrap" height="100%">
          <SidebarCloser
            navCollapsed={navCollapsed}
            onClick={this.onSetSidebarOpen}
          />
          <Sidebar navCollapsed={navCollapsed} />
          <Box w={1}>
            <Header
              onSetSidebarOpen={this.onSetSidebarOpen}
              navCollapsed={navCollapsed}
              windowWidth={windowWidth}
            />
            <Flex
              px={10}
              py={4}
              overflow="auto"
              position="relative"
              height="calc(100% - 64px)"
              flexDirection="column"
            >
              <Container left>{children}</Container>
            </Flex>
          </Box>
        </Flex>
      </Wrapper>
    );
  }
}

PrimaryLayout.propTypes = {
  children: PropTypes.object.isRequired,
};

/**
 * PublicLayout
 * @returns {object}.
 */
export const PublicLayout = ({ children }) => (
  <WrapperPublic>
    <Container>
      <HeaderPublic />
      <Flex alignItems="center" justifyContent="center" px={[4, 10]}>
        <Box w={[1, 1, 10 / 12, 8 / 12]}>
          <Panel>
            <Flex>
              <Box w={[1, 2 / 3]} px={[8, 14]} py={8} position="relative">
                {children}
              </Box>
              <Box w={1 / 3} display={['none', 'block']}>
                <Logo to="/" />
              </Box>
            </Flex>
          </Panel>
        </Box>
      </Flex>
    </Container>
  </WrapperPublic>
);

PublicLayout.propTypes = {
  children: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
};
