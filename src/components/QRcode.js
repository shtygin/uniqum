import React, { Component } from 'react';
import PropTypes from 'prop-types';
import qrcode from 'qrcode';

/**
 * Generate and draw QR code.
 * @param {object} values values.
 */
class QRcode extends Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);

    this.state = {
      value: null,
    };
  }

  /**
   * Life cycle.
   * @param {object} nextProps new props.
   */
  componentWillReceiveProps(nextProps) {
    const { size, value } = this.props;
    const opts = {
      width: Number(size),
      errorCorrectionLevel: 'H',
    };

    if (value || nextProps.value) {
      qrcode.toDataURL(value || nextProps.value, opts, (err, src) => {
        if (err) {
          return console.error(err);
        }
        return this.setState({ value: src });
      });
    }
  }

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const { value } = this.state;
    const { size } = this.props;

    return <img src={value} width={size} height={size} alt="" />;
  }
}

QRcode.propTypes = {
  size: PropTypes.number,
  value: PropTypes.string.isRequired,
};

QRcode.defaultProps = {
  size: 100,
};

export default QRcode;
