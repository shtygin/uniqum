import React from 'react';

// styled
import styled from 'styled-components';
import colors from 'UIKit/Colors';

const LoadingContainer = styled.div`
  position: absolute;
  z-index: 1;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: ${colors.white['750']};
`;
const LoadingIcon = styled.svg`
  position: absolute;
  top: 50%;
  left: 50%;
  width: 32px;
  height: 32px;
  transform: translate(-50%, -50%);
`;
const LoadingText = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  margin-top: 32px;
  color: rgba(0, 0, 0, 0.87);
  font-weight: 500;
  transform: translate(-50%, -50%);
`;

/**
 * Loading.
 * @returns {*}
 * @param {object} props props.
 */
const Loading = props => {
  const { children } = props; // eslint-disable-line react/prop-types
  return (
    <LoadingContainer>
      <LoadingIcon viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
        <circle
          cx="50"
          cy="50"
          fill="none"
          stroke="#00D0BD"
          strokeWidth="8"
          r="24"
          strokeDasharray="113.09733552923255 39.69911184307752"
          transform="rotate(209.934 50 50)"
        >
          <animateTransform
            attributeName="transform"
            type="rotate"
            calcMode="linear"
            values="0 50 50;360 50 50"
            keyTimes="0;1"
            dur="0.8s"
            begin="0s"
            repeatCount="indefinite"
          />
        </circle>
      </LoadingIcon>
      <LoadingText>{children}</LoadingText>
    </LoadingContainer>
  );
};

export default Loading;
