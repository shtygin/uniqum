import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { actions } from 'modules/Project';

const adaptFileEventToValue = delegate => e => delegate(e.target.files[0]);

/**
 * FileInput.
 * @param {object} values values.
 */
class FileInput extends React.PureComponent {
  /**
   * handleChange
   * @param {object} event event
   */
  handleChange = event => {
    const { onUpload } = this.props;
    const file = event.target.files[0];
    onUpload(file);
  };

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const {
      input,
      input: { onBlur },
    } = this.props;

    return (
      <input
        onChange={event => this.handleChange(event)}
        onBlur={adaptFileEventToValue(onBlur)}
        type="file"
      />
    );
  }
}

FileInput.propTypes = {
  input: PropTypes.object.isRequired,
  onUpload: PropTypes.func.isRequired,
};

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  onUpload(file) {
    dispatch(actions.fetchUpload(file));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FileInput);
