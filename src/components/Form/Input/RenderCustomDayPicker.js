import PropTypes from 'prop-types';
import React, { Fragment } from 'react';
import DayPicker, { DateUtils } from 'react-day-picker';

import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';

import 'react-day-picker/lib/style.css';

export const Error = styled.div`
  font-size: 0.875rem;
  color: ${({ warning }) => (warning ? 'orange' : '#f55145')};
  &:not(:first-child) {
    margin-top: 4px;
  }
`;

const WEEKDAYS_SHORT = {
  ru: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
};
const MONTHS = {
  ru: [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь',
  ],
};

const WEEKDAYS_LONG = {
  ru: [
    'Воскресенье',
    'Понедельник',
    'Вторник',
    'Среда',
    'Четверг',
    'Пятница',
    'Суббота',
  ],
};

const FIRST_DAY_OF_WEEK = {
  ru: 1,
};
// Translate aria-labels
const LABELS = {
  ru: { nextMonth: 'следующий месяц', previousMonth: 'предыдущий месяц' },
};

/**
 * RenderCustomDayPicker.
 */
class RenderCustomDayPicker extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    this.state = {
      selectedDays: [],
    };
  }

  /**
   * Handle.
   * @param {String} day props.
   * @param {Object} values values.
   */
  handleDayClick = (day, values) => {
    const { selected } = values;
    const {
      input: { onChange },
    } = this.props;
    const { selectedDays } = this.state;
    if (selected) {
      const selectedIndex = selectedDays.findIndex(selectedDay =>
        DateUtils.isSameDay(selectedDay, day)
      );
      selectedDays.splice(selectedIndex, 1);
    } else {
      selectedDays.push(day);
    }
    this.setState({ selectedDays }, () => {
      onChange(selectedDays);
    });
  };

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const { selectedDays } = this.state;
    const {
      locale,
      meta: { touched, error, warning },
    } = this.props;
    return (
      <Fragment>
        <DayPicker
          locale={locale}
          months={MONTHS[locale]}
          weekdaysLong={WEEKDAYS_LONG[locale]}
          weekdaysShort={WEEKDAYS_SHORT[locale]}
          firstDayOfWeek={FIRST_DAY_OF_WEEK[locale]}
          labels={LABELS[locale]}
          selectedDays={selectedDays}
          onDayClick={this.handleDayClick}
        />
        {touched &&
          ((error && (
            <Error>
              <FormattedMessage id={error} defaultMessage="" />
            </Error>
          )) ||
            (warning && <Error warning>{warning}</Error>))}
      </Fragment>
    );
  }
}

RenderCustomDayPicker.propTypes = {
  meta: PropTypes.object.isRequired,
  input: PropTypes.object.isRequired,
  locale: PropTypes.string.isRequired,
};

export default RenderCustomDayPicker;
