import { injectGlobal } from 'styled-components/typings/styled-components';

const frw = require('./SuisseIntl-Regular.woff');
const frw2 = require('./SuisseIntl-Regular.woff2');
const fbw = require('./SuisseIntl-Bold.woff');
const fbw2 = require('./SuisseIntl-Bold.woff2');

injectGlobal`
  @font-face {
    font-family: SuisseIntl;
    src: url(${frw2}) format('woff2'), url(${frw}) format('woff');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: SuisseIntl;
    src: url(${fbw2}) format('woff2'), url(${fbw}) format('woff');
    font-weight: 700;
    font-style: normal;
  }
`;
