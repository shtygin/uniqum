import { injectGlobal, css } from 'styled-components';
import normalizeCSS from 'normalize.css';
import colors from 'UIKit/Colors';

// eslint-disable-next-line  no-unused-expressions
injectGlobal`
  ${css`
    ${normalizeCSS};
  `}
  
  html,
  body {
    overflow: hidden;
    height: 100%;
    width: 100%;
    padding: 0;
    margin: 0;
    font-family: 'Helvetica Neue', arial, sans-serif;
    color: ${colors.gray['900']};
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  html {
    font-size: 16px;
  }
  
  body {
    background-color: ${colors.blue['100']};
  }

  * {
    box-sizing: border-box;
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);

    &:active,
    &:focus,
    &:visited {
      outline: none;
    }
  }
  
  #root {
    height: 100%;
  }

  a {
    color: inherit;
    text-decoration: none;
    outline: none;
    cursor: pointer;
  }

  input, textarea, button {
    font-family: 'Helvetica Neue', arial, sans-serif;
  }

  button {
    cursor: pointer;
    border-radius: 0;
  }

  #app {
    height: 100%;
  }
`;
