import styled from 'styled-components';
import mq from 'UIKit/Mq';

const Container = styled.div`
  width: 100%;
  margin: 0 ${({ left }) => (left ? '0' : 'auto')};
  ${mq.desktop1280`
  	max-width: ${({ noFull }) => (noFull ? '152rem' : 'none')};
  `};
`;

export default Container;
