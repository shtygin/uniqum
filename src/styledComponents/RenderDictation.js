import React from 'react';
import PropTypes from 'prop-types';

import RenderAnswerCounter from 'styledComponents/RenderDictation/RenderAnswerCounter';
import RenderColumn from 'styledComponents/RenderDictation/RenderColumn';
import ControlPanel from '../../vendors/simulators/src/ControlPanel';
import DictationTime from '../../vendors/simulators/src/DictationTime';
import AnswerCounter from '../../vendors/simulators/src/AnswerCounter';
import LinearProgressCustom from '../../vendors/simulators/src/LinearProgressCustom';
import Column from '../../vendors/simulators/src/Column';

// <Grid item xs={2} lg={3} sm={3} md={3}>
//   <>
//     <IconButton
//       aria-owns={openEl ? 'menu-appbar' : null}
//       aria-haspopup="true"
//       color="inherit"
//       onClick={handleMenu}
//     >
//       <FontAwesomeIcon icon={faCog} />
//     </IconButton>
//     <Menu
//       id="menu-appbar"
//       anchorEl={AnchorEl}
//       anchorOrigin={{
//         vertical: 'bottom',
//         horizontal: 'right',
//       }}
//       getContentAnchorEl={null}
//       transformOrigin={{
//         vertical: 'top',
//         horizontal: 'left',
//       }}
//       open={openEl}
//       onClose={handleMenuClose}
//     >
//       <MenuItem key={0}>
//         <IconButton onClick={handleMenuClose}>
//           <FontAwesomeIcon icon={faTimesCircle} />
//         </IconButton>
//       </MenuItem>
//       {visible.settings && (
//         <MenuItem key={1}>
//           <FormControlLabel
//             value="end"
//             control={
//               <Checkbox
//                 color="primary"
//                 checked={open}
//                 onChange={handleParamsBlock}
//               />
//             }
//             label="Параметры"
//             labelPlacement="end"
//           />
//         </MenuItem>
//       )}
//       {visible.sample && (
//         <MenuItem key={2}>
//           <FormControlLabel
//             value="end"
//             control={
//               <Checkbox
//                 color="primary"
//                 checked={sample}
//                 onChange={event => onToggleSetting(event, 'sample')}
//               />
//             }
//             label="Показать пример(ы)"
//             labelPlacement="end"
//           />
//         </MenuItem>
//       )}
//       {visible.showAnswers && (
//         <MenuItem key={3}>
//           <FormControlLabel
//             value="end"
//             control={
//               <Checkbox
//                 color="primary"
//                 checked={showAnswers}
//                 onChange={event => onToggleSetting(event, 'showAnswers')}
//               />
//             }
//             label="Показать ответ(ы)"
//             labelPlacement="end"
//           />
//         </MenuItem>
//       )}
//       {visible.abacus && (
//         <MenuItem key={4}>
//           <FormControlLabel
//             value="end"
//             control={
//               <Checkbox
//                 color="primary"
//                 checked={abacus}
//                 onChange={event => onToggleSetting(event, 'abacus')}
//               />
//             }
//             label="Виртуальный соробан"
//             labelPlacement="end"
//           />
//         </MenuItem>
//       )}
//       {room === 'roomA' && visible.sound && (
//         <MenuItem key={5}>
//           <FormControlLabel
//             value="end"
//             control={
//               <Checkbox
//                 color="primary"
//                 checked={sound}
//                 onChange={event => onToggleSetting(event, 'sound')}
//               />
//             }
//             label="Звук"
//             labelPlacement="end"
//           />
//         </MenuItem>
//       )}
//     </Menu>
//   </>
// </Grid>

const RenderDictation = ({
  state,
  room,
  dictationState,
  onShowParamsBlock,
  onToggleSetting,
  onChangeDictationSetting,
}) => {
  const { visible } = state;
  return (
    <>
      {dictationState.dictation && dictationState.progress && (
        <LinearProgressCustom period={dictationState.period} />
      )}
      <Column render={props => <RenderColumn {...props} />} />
      {dictationState.dictation ? (
        <DictationTime
          onChange={onChangeDictationSetting}
          period={dictationState.period}
          samples={dictationState.samples}
        />
      ) : (
        <ControlPanel />
      )}
      <AnswerCounter
        onShowParamsBlock={onShowParamsBlock}
        visible={visible.answerCounter}
        room={room}
        onToggleSetting={onToggleSetting}
        render={props => <RenderAnswerCounter {...props} />}
      />
    </>
  );
};

RenderDictation.propTypes = {
  onChangeParams: PropTypes.func.isRequired,
  state: PropTypes.object.isRequired,
  dictationState: PropTypes.object.isRequired,
  onToggleSetting: PropTypes.func.isRequired,
  onShowParamsBlock: PropTypes.func.isRequired,
  onChangeDictationSetting: PropTypes.func.isRequired,
  room: PropTypes.string.isRequired,
};

export default RenderDictation;
