import React from 'react';
import PropTypes from 'prop-types';

const RenderColumn = ({ state }) => {
  const {
    controls: { check, value },
    args,
    request,
  } = state;

  let i = 0;

  const sum = args.reduce((acc, next) => acc + next, 0);
  const size = 30 / args.length;
  // const sizeAnswer = Math.round(30 / String(sum).length);
  let color = 'green';
  if (Number(value) !== sum) {
    color = 'red';
  }

  return (
    <div>
      {check ? (
        <div style={{ color, fontSize: `10vh` }}>{sum}</div>
      ) : (
        <>
          {args.map(num => (
            <div key={(i += 1)} style={{ fontSize: `${size}vh` }}>
              {request.success && num}
            </div>
          ))}
        </>
      )}
    </div>
  );
};

RenderColumn.propTypes = {
  state: PropTypes.object.isRequired,
};

export default RenderColumn;
