import React from 'react';
import PropTypes from 'prop-types';

// import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import IconButton from '@material-ui/core/IconButton';
// import Badge from '@material-ui/core/Badge';
// import { withStyles } from '@material-ui/core/styles';
import {
  // faCog,
  // faThumbsUp,
  // faThumbsDown,
  faPlayCircle,
  faPauseCircle,
  faSpinner,
  // faTimesCircle,
} from '@fortawesome/free-solid-svg-icons';

// const StyledBadge = withStyles(theme => ({
//   badge: {
//     right: -5,
//     border: `2px solid ${theme.palette.grey[200]}`,
//   },
// }))(Badge);

const Play = ({ start, stop, onDictationStart, state }) => {
  const { controls, iconmin } = state;
  const handleStart = () => {
    start();
    onDictationStart();
  };

  const handleStop = () => {
    stop();
  };

  let component = (
    <Button
      color="primary"
      variant="contained"
      onClick={handleStart}
      startIcon={<FontAwesomeIcon icon={faPlayCircle} />}
    >
      {!iconmin && 'Старт'}
    </Button>
  );

  if (controls.start) {
    component = (
      <Button
        color="primary"
        variant="contained"
        onClick={handleStop}
        startIcon={<FontAwesomeIcon icon={faPauseCircle} />}
      >
        {!iconmin && 'Стоп'}
      </Button>
    );
  }

  return component;
};

const RenderAnswerCounter = ({
  state,
  visible,
  start,
  stop,
  onDictationStart,
}) => {
  const {
    request,
    controls: { success, failure },
    iconmin,
  } = state;
  return (
    <>
      {visible.play && (
        <>
          {request && request.loading ? (
            <Button
              color="primary"
              variant="contained"
              disabled
              endIcon={<FontAwesomeIcon spin icon={faSpinner} />}
            >
              {!iconmin && 'Загрузка'}
            </Button>
          ) : (
            <Play
              start={start}
              stop={stop}
              onDictationStart={onDictationStart}
              state={state}
            />
          )}
        </>
      )}
      <span style={{ color: 'green' }}>{success}</span>
      <span style={{ color: 'red' }}>{failure}</span>
    </>
  );
};

RenderAnswerCounter.propTypes = {
  stop: PropTypes.func.isRequired,
  start: PropTypes.func.isRequired,
  onDictationStart: PropTypes.func.isRequired,
  state: PropTypes.object.isRequired,
  visible: PropTypes.object.isRequired,
};

export default RenderAnswerCounter;
