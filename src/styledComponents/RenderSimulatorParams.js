import React from 'react';
import PropTypes from 'prop-types';

import { Box, Flex } from 'UIKit/Grid';

const RenderSimulatorParams = ({ onChangeParams, params }) => {
  const handleChange = (event, key) => {
    const newParams = { ...params };
    onChangeParams({ ...newParams, [key]: event.target.value });
  };

  return (
    <Flex>
      <Box>
        <div>Интервал</div>
        <input
          type="text"
          value={params.steps}
          name="interval"
          onChange={event => handleChange(event, 'steps')}
        />
      </Box>
      <Box>
        <div>Правило</div>
        <select name="mode" onChange={event => handleChange(event, 'mode')}>
          <option value={1}>5</option>
          <option value={2}>10</option>
          <option value={3}>5 + 10</option>
        </select>
      </Box>
    </Flex>
  );
};

RenderSimulatorParams.propTypes = {
  onChangeParams: PropTypes.func.isRequired,
  params: PropTypes.object.isRequired,
};

export default RenderSimulatorParams;
