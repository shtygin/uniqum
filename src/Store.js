import { createStore, combineReducers, applyMiddleware } from 'redux';

import userReducer from 'modules/User';
import socketReducer from 'modules/Socket';
import requestsReducer from 'modules/Requests';
import simulatorsrReducer from 'modules/Simulators';
import homeworksrReducer from 'modules/Homework';
import arifmetikBookReducer from 'modules/ArifmetikBook';
import notifierReducer from 'modules/Notifier';

import { reducer as formReducer } from 'redux-form';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { apiMiddleware } from 'redux-api-middleware';
import WebSocket from 'utils/middleware/WebSocket';
import ApiAuthInjector from 'utils/middleware/AuthApiInjector';
import Requests from 'utils/middleware/Requests';
import Forms from 'utils/middleware/Forms';

const middleware = [
  ApiAuthInjector,
  apiMiddleware,
  WebSocket,
  Requests,
  Forms,
  thunk,
];

if (process.env.NODE_ENV !== 'production') {
  middleware.push(createLogger({ collapsed: true }));
}

const rootReducer = combineReducers({
  form: formReducer,
  user: userReducer,
  socket: socketReducer,
  requests: requestsReducer,
  homeworks: homeworksrReducer,
  simulators: simulatorsrReducer,
  notifier: notifierReducer,
  arifmetikBook: arifmetikBookReducer,
});

const store = createStore(rootReducer, applyMiddleware(...middleware));

export default store;
