import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withSnackbar } from 'notistack';
import { bindActionCreators } from 'redux';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';
import { actions } from 'modules/Notifier';

/**
 * Notifier
 * @param {string} id id.
 */
class Notifier extends Component {
  displayed = [];

  /**
   * Life cycle.
   */
  componentDidUpdate() {
    const { notifications = [], enqueueSnackbar, removeSnackbar } = this.props;

    notifications.forEach(notification => {
      if (this.displayed.includes(notification.key)) return;
      enqueueSnackbar(notification.message, {
        ...{ ...notification.options, autoHideDuration: 15000 },
        action: this.action,
      });

      this.storeDisplayed(notification.key);
      removeSnackbar(notification.key);
    });
  }

  /**
   * @param {string} id клюю нотификации.
   */
  storeDisplayed = id => {
    this.displayed = [...this.displayed, id];
  };

  /**
   * @param {string} key ключ нотификации.
   * @returns {object}
   */
  action = key => {
    const { closeSnackbar } = this.props;
    return (
      <Button
        onClick={() => {
          closeSnackbar(key);
        }}
      >
        <CloseIcon style={{ color: 'white' }} />
      </Button>
    );
  };

  /**
   * Render
   * @returns {boolean}.
   */
  render() {
    return null;
  }
}

Notifier.propTypes = {
  notifications: PropTypes.array.isRequired,
  enqueueSnackbar: PropTypes.func.isRequired,
  removeSnackbar: PropTypes.func.isRequired,
  closeSnackbar: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  notifications: state.notifier.notifications,
});

// const mapDispatchToProps = dispatch => ({
//   removeSnackbar() {
//     dispatch(actions.removeSnackbar());
//   },
//   enqueueSnackbar() {
//     dispatch(actions.enqueueSnackbar());
//   },
// });

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      removeSnackbar: actions.removeSnackbar,
    },
    dispatch
  );

export default withSnackbar(
  connect(mapStateToProps, mapDispatchToProps)(Notifier)
);
