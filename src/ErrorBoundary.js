import React from 'react';
import PropTypes from 'prop-types';

/**
 * ErrorBoundary handle.
 * @param {object} values values.
 */
class ErrorBoundary extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  /**
   * Life cycle.
   * @param {object} error - error.
   * @param {object} info - info.
   */
  componentDidCatch(error, info) {
    // Display fallback UI
    this.setState({ hasError: true });
    if (typeof error === 'object') {
      console.error(JSON.stringify(error), info);
    } else {
      console.error(error, info);
    }
    // You can also log the error to an error reporting service
    console.error(error, info);
  }

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const { hasError } = this.state;
    const { children } = this.props;
    if (hasError) {
      window.location.href = '/simulators';
    }
    return children;
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.any.isRequired,
};

export default ErrorBoundary;
