const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const isProduction = process.env.NODE_ENV === 'production';

const ROOT_DIR = __dirname;
const config = {
  output: {
    frontend: path.resolve(ROOT_DIR, 'public'),
  },
  service_name: 'Uniqumkids',
  pages_title: 'Uniqumkids',
  recaptcha: {
    public: '6Lf0J9kUAAAAAG9wnYwwpiXSda4FcPIVF3AciXi1',
  },
};

const htmlOptions = {
  removeAttributeQuotes: true,
  collapseWhitespace: true,
  html5: true,
  minifyCSS: true,
  minifyJS: true,
  removeComments: true,
  removeEmptyAttributes: true,
};

const plugins = [];
const loaders = [];


plugins.push(
  new HtmlWebpackPlugin({
    filename: path.resolve(config.output.frontend, 'index.html'),
    template: path.resolve(ROOT_DIR, 'src/index.html'),
    title: config.pages_title,
    minify: isProduction ? htmlOptions : false,
  })
);

plugins.push(new ScriptExtHtmlWebpackPlugin({ defaultAttribute: 'defer' }));

plugins.push(
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      RECAPTCHA_PUBLIC: JSON.stringify(config.recaptcha.public),
      SERVICE_NAME: JSON.stringify(
        process.env.SERVICE_NAME || config.service_name
      ),
      BACKEND_URL: JSON.stringify(process.env.BACKEND_URL),
      WEBSOCKET_URL: JSON.stringify(process.env.WEBSOCKET_URL),
    },
  })
);

if (isProduction) {
  plugins.push(
    new UglifyJsPlugin({
      parallel: true,
      sourceMap: false,
      uglifyOptions: {
        ecma: 6,
        compress: {
          drop_console: true,
        },
        output: {
          beautify: false,
          comments: false,
        },
      },
    })
  );
}

if (!isProduction) {
  loaders.push({
    test: /(\.jsx|\.js)$/,
    enforce: 'pre',
    exclude: /node_modules/,
    resolve: { extensions: ['.js', '.jsx'] },
    loader: "eslint-loader",
    options: {
     fix: true, 
    },
  });
}

loaders.push({
  test: /(\.jsx|\.js)$/,
  exclude: /node_modules/,
  use: 'babel-loader',
});

loaders.push({
  test: /\.scss$/,
  use: ['style-loader', 'css-loader', 'sass-loader'],
});

loaders.push({
  test: /\.css$/,
  loader: 'css-loader',
});

loaders.push({
  test: /\.(png|jpg|gif)$/,
  use: [
    {
      loader: 'file-loader',
      options: {},
    },
  ],
});

module.exports = {
  mode: isProduction ? 'production' : 'development',
  context: path.resolve(ROOT_DIR, 'src'),
  devtool: isProduction ? false : 'cheap-module-source-map',

  entry: {
    app: './index.js',
  },

  output: {
    filename: '[name].[chunkhash].js',
    chunkFilename: '[name].[id].[chunkhash].js',
    publicPath: '/',
    path: config.output.frontend,
    pathinfo: false,
  },
  resolve: {
    modules: [path.resolve(ROOT_DIR, 'src'), 'node_modules'],
    alias: {
      root: ROOT_DIR,
      components: path.resolve(ROOT_DIR, 'src/components'),
      styledComponents: path.resolve(ROOT_DIR, 'src/styledComponents'),
      modules: path.resolve(ROOT_DIR, 'src/modules'),
      utils: path.resolve(ROOT_DIR, 'src/utils'),
      pages: path.resolve(ROOT_DIR, 'src/pages'),
      actions: path.resolve(ROOT_DIR, 'src/actions'),
      config: path.resolve(ROOT_DIR, 'src/config'),
      UIKit: path.resolve(ROOT_DIR, 'src/components/UIKit'),
    },
  },
  module: {
    rules: loaders,
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /node_modules/,
          name: 'vendor',
          chunks: 'all',
        },
      },
    },
  },
  devServer: {
    historyApiFallback: true,
    contentBase: config.output.frontend,
  },
  plugins,
  stats: isProduction ? 'errors-only' : 'normal',
};
