import React, { useContext, Fragment, useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMinus, faPlus } from '@fortawesome/free-solid-svg-icons';
import { makeStyles } from '@material-ui/core/styles';

import { interval as rxInterval, concat } from 'rxjs';
import {
  takeWhile,
  takeUntil,
  scan,
  startWith,
  tap,
  switchMap,
  delay,
  endWith,
  filter,
} from 'rxjs/operators';

import { useEventCallback } from 'rxjs-hooks';
import { fillingValues } from './utils/helpers';
import Abacus from './menar/Abacus';
import { PersonalPanelContext } from './PersonalPanelContext';
import usePersonalControl from './usePersonalControl';

const useStyles = makeStyles(() => ({
  sign: {
    marginBottom: '20px',
    fontSize: '40px',
    color: '#022d63',
  },
}));

function tmrEpic(events$) {
  return events$.pipe(
    filter(e => e.type === 'timer'),
    tap(action => action.deps.progress()),
    switchMap(action =>
      concat(
        rxInterval(1000 * Number(action.payload.interval)).pipe(
          startWith(0),
          tap(() => action.deps.setHidden(true)),
          delay(10),
          tap(() => action.deps.setHidden(false)),
          scan(time => time + 1),

          takeWhile(time => time < action.payload.length),

          endWith(() => action.deps.end()),
          takeUntil(events$.pipe(filter(e => e.type === 'stop')))
        )
      )
    )
  );
}

function onceEpic(events$) {
  return events$.pipe(
    filter(e => e.type === 'once'),
    tap(action => action.deps.begin())
  );
}

const FlashCard = () => {
  const [state] = useContext(PersonalPanelContext);
  const classes = useStyles();

  // TODO упростить компонент

  const {
    params: { interval },
    args,
    request,
    workout,
    controls: { start, check, value, stop },
  } = state;

  const { begin, progress, end } = usePersonalControl();
  const [color, setColor] = useState('black');

  const sum = args.reduce((acc, next) => acc + next, 0);
  const size = Math.round(30 / String(sum).length);

  useEffect(() => {
    if (check) {
      setColor('green');
      if (Number(value) !== sum) {
        setColor('red');
      }
    }
  }, [value, sum, check]);

  const [abacus, setAbacus] = useState({});
  const [hidden, setHidden] = useState(false);
  const [onEvent, index] = useEventCallback(tmrEpic);

  useEffect(() => {
    const newAbacus = fillingValues(
      Math.abs(args[index]),
      String(Math.abs(args[index])).length
    );
    setAbacus(newAbacus);
  }, [args, index]);

  const [onOnce] = useEventCallback(onceEpic);

  useEffect(() => {
    if (request.success && args.length > 0 && start) {
      onOnce({ type: 'once', payload: { workout }, deps: { begin } });
    }
  }, [args.length, start, request.success]);

  useEffect(() => {
    if (workout.begin) {
      onEvent({
        type: 'timer',
        payload: { length: args.length, interval },
        deps: { progress, end, setColor, setHidden },
      });
    }
  }, [args.length, interval, onEvent, workout.begin]);

  useEffect(() => {
    if (stop) {
      onEvent({ type: 'stop' });
    }
  }, [onEvent, stop]);

  const sumAllRod = Object.values(abacus).reduce(
    (acc, column) => acc + column.reduce((a, b) => a + b, 0),
    0
  );

  return (
    <Grid item xs={12} lg={12} sm={12} md={12}>
      {check ? (
        <div style={{ color, fontSize: `${size}vh` }}>{sum}</div>
      ) : (
        <Fragment>
          {workout.progress ? (
            <>
              {workout.progress && (
                <div className={classes.sign}>
                  {args[index] < 0 ? (
                    <FontAwesomeIcon
                      color={hidden ? 'white' : ''}
                      icon={faMinus}
                    />
                  ) : (
                    <FontAwesomeIcon
                      color={hidden ? 'white' : ''}
                      icon={faPlus}
                    />
                  )}
                </div>
              )}
              {sumAllRod > 0 && (
                <Abacus
                  abacus={abacus}
                  hidden={hidden}
                  handleChange={() => console.log('click')}
                />
              )}
            </>
          ) : (
            <div style={{ fontSize: `${size}vh` }}>{workout.end && <>?</>}</div>
          )}
        </Fragment>
      )}
    </Grid>
  );
};

export default FlashCard;
