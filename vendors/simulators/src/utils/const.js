// export const coefficient = {
//   mode: { xs: 2, sm: 2, md: 2, lg: 2 },
//   numbers: { xs: 3, sm: 2, md: 2, lg: 2 },
//   bound: { xs: 2, sm: 2, md: 2, lg: 2 },
//   interval: { xs: 2, sm: 2, md: 2, lg: 2 },
//   steps: { xs: 2, sm: 2, md: 2, lg: 2 },
//   range: { xs: 2, sm: 2, md: 1, lg: 1 },
//   sign: { xs: 2, sm: 2, md: 1, lg: 1 },
//   buttonCheck: {
//     xs: 2.5,
//     sm: 1,
//     md: 2,
//     lg: 2,
//   },
// };

export const coefficient = {
  mode: { xs: 2, sm: 2, md: 2, lg: 2 },
  section: { xs: 3, sm: 2, md: 2, lg: 2 },
  numbers: { xs: 3, sm: 2, md: 2, lg: 2 },
  bound: { xs: 2, sm: 2, md: 2, lg: 2 },
  interval: { xs: 2, sm: 2, md: 2, lg: 1 },
  steps: { xs: 2, sm: 2, md: 2, lg: 1 },
  sign: { xs: 2, sm: 2, md: 1, lg: 1 },
  under: { xs: 2, sm: 2, md: 2, lg: 2 },
  buttonCheck: {
    xs: 2.5,
    sm: 1,
    md: 2,
    lg: 2,
  },
};

export const NO_MODE = 0;
export const RULE_SIMPLE_SUM = 1;
export const RULE_FIVE_SUM = 2;
export const RULE_TEN_SUM = 3;
export const RULE_FIVE_AND_TEN_SUM = 4;
export const SECTION_UP_TO_FOUR = 4;
export const SECTION_UP_TO_FIVE = 5;
export const SECTION_UP_TO_SIX = 6;
export const SECTION_UP_TO_SEVEN = 7;
export const SECTION_UP_TO_EIGHT = 8;
export const SECTION_UP_TO_NINE = 9;
export const SECTION_MIRROR = 2;
export const SECTION_TEN = 1;

export const xs = 3;
export const sm = 2;
export const md = 1;
export const lg = 1;

export const SOUND_URL = 'http://lk.gorazd.online';

export const formulas = {
  '0': {
    '0': {
      '1': '1 = 5 - 4, 1 = 10 - 9, 1 = 10 - 5 - 4',
      '2': '2 = 5 - 3, 2 = 10 - 8, 2 = 10 - 5 - 3',
      '3': '3 = 5 - 2, 3 = 10 - 7, 3 = 10 - 5 - 2',
      '4': '4 = 5 - 1, 4 = 10 - 6, 4 = 10 - 5 - 1',
      '5': '5 = 10 - 5',
      '6': '6 = 5 + 1, 6 = 10 - 4, 6 = 10 - 5 + 1',
      '7': '7 = 5 + 2, 7 = 10 - 3, 7 = 10 - 5 + 2',
      '8': '8 = 5 + 3, 8 = 10 - 2, 8 = 10 - 5 + 3',
      '9': '9 = 5 + 4, 9 = 10 - 1, 9 = 10 - 5 + 4',
    },
    '1': {},
    '2': {
      '1': '1 = 5 - 4',
      '2': '2 = 5 - 3',
      '3': '3 = 5 - 2',
      '4': '4 = 5 - 1',
    },
    '3': {
      '1': '1 = 10 - 9',
      '2': '2 = 10 - 8',
      '3': '3 = 10 - 7',
      '4': '4 = 10 - 6',
      '5': '5 = 10 - 5',
      '6': '6 = 10 - 4',
      '7': '7 = 10 - 3',
      '8': '8 = 10 - 2',
      '9': '9 = 10 - 1',
    },
    '4': {
      '6': '6 = 10 - 5 + 1',
      '7': '7 = 10 - 5 + 2',
      '8': '8 = 10 - 5 + 3',
      '9': '9 = 10 - 5 + 4',
    },
  },
  '1': {
    '0': {
      '1': '-1 = -5 + 4, -1 = -10 + 9, -1 = -10 + 5 + 4',
      '2': '-2 = -5 + 3, -2 = -10 + 8, -2 = -10 + 5 + 3',
      '3': '-3 = -5 + 2, -3 = -10 + 7, -3 = -10 + 5 + 2',
      '4': '-4 = -5 + 1, -4 = -10 + 6, -4 = -10 + 5 + 1',
      '5': '-5 = -10 + 5',
      '6': '-6 = -5 - 1, -6 = -10 + 4, -6 = -10 + 5 - 1',
      '7': '-7 = -5 - 2, -7 = -10 + 3, -7 = -10 + 5 - 2',
      '8': '-8 = -5 - 3, -8 = -10 + 2, -8 = -10 + 5 - 3',
      '9': '-9 = -5 - 4, -9 = -10 + 1, -9 = -10 + 5 - 4',
    },
    '1': {},
    '2': {
      '1': '-1 = -5 + 4',
      '2': '-2 = -5 + 3',
      '3': '-3 = -5 + 2',
      '4': '-4 = -5 + 1',
    },
    '3': {
      '1': '-1 = -10 + 9',
      '2': '-2 = -10 + 8',
      '3': '-3 = -10 + 7',
      '4': '-4 = -10 + 6',
      '5': '-5 = -10 + 5',
      '6': '-6 = -10 + 4',
      '7': '-7 = -10 + 3',
      '8': '-8 = -10 + 2',
      '9': '-9 = -10 + 1',
    },
    '4': {
      '6': '-6 = -10 + 5 - 1',
      '7': '-7 = -10 + 5 - 2',
      '8': '-8 = -10 + 5 - 3',
      '9': '-9 = -10 + 5 - 4',
    },
  },
  '2': {
    '0': {
      '1':
        '&#177;1 = &#177;5 <span class="rotate">&#177;</span> 4, &#177;1 = &#177;10 <span class="rotate">&#177;</span> 9, &#177;1 = &#177;10 <span class="rotate">&#177;</span> 5 <span class="rotate">&#177;</span> 4',
      '2':
        '&#177;2 = &#177;5 <span class="rotate">&#177;</span> 3, &#177;2 = &#177;10 <span class="rotate">&#177;</span> 8, &#177;2 = &#177;10 <span class="rotate">&#177;</span> 5 <span class="rotate">&#177;</span> 3',
      '3':
        '&#177;3 = &#177;5 <span class="rotate">&#177;</span> 2, &#177;3 = &#177;10 <span class="rotate">&#177;</span> 7, &#177;3 = &#177;10 <span class="rotate">&#177;</span> 5 <span class="rotate">&#177;</span> 2',
      '4':
        '&#177;4 = &#177;5 <span class="rotate">&#177;</span> 1, &#177;4 = &#177;10 <span class="rotate">&#177;</span> 6, &#177;4 = &#177;10 <span class="rotate">&#177;</span> 5 <span class="rotate">&#177;</span> 1',
      '5': '&#177;5 = &#177;10 + 5',
      '6':
        '&#177;6 = &#177;5 &#177; 1, &#177;6 = &#177;10 <span class="rotate">&#177;</span> 4, &#177;6 = &#177;10 <span class="rotate">&#177;</span> 5 &#177; 1',
      '7':
        '&#177;7 = &#177;5 &#177; 2, &#177;7 = &#177;10 <span class="rotate">&#177;</span> 3, &#177;7 = &#177;10 <span class="rotate">&#177;</span> 5 &#177; 2',
      '8':
        '&#177;8 = &#177;5 &#177; 3, &#177;8 = &#177;10 <span class="rotate">&#177;</span> 2, &#177;8 = &#177;10 <span class="rotate">&#177;</span> 5 &#177; 3',
      '9':
        '&#177;9 = &#177;5 &#177; 4, &#177;9 = &#177;10 <span class="rotate">&#177;</span> 1, &#177;9 = &#177;10 <span class="rotate">&#177;</span> 5 &#177; 4',
    },
    '1': {},
    '2': {
      '1': '&#177;1 = &#177;5 <span class="rotate">&#177;</span> 4',
      '2': '&#177;2 = &#177;5 <span class="rotate">&#177;</span> 3',
      '3': '&#177;3 = &#177;5 <span class="rotate">&#177;</span> 2',
      '4': '&#177;4 = &#177;5 <span class="rotate">&#177;</span> 1',
    },
    '3': {
      '1': '&#177;1 = &#177;10 <span class="rotate">&#177;</span> 9',
      '2': '&#177;2 = &#177;10 <span class="rotate">&#177;</span> 8',
      '3': '&#177;3 = &#177;10 <span class="rotate">&#177;</span> 7',
      '4': '&#177;4 = &#177;10 <span class="rotate">&#177;</span> 6',
      '5': '&#177;5 = &#177;10 <span class="rotate">&#177;</span> 5',
      '6': '&#177;6 = &#177;10 <span class="rotate">&#177;</span> 4',
      '7': '&#177;7 = &#177;10 <span class="rotate">&#177;</span> 3',
      '8': '&#177;8 = &#177;10 <span class="rotate">&#177;</span> 2',
      '9': '&#177;9 = &#177;10 <span class="rotate">&#177;</span> 1',
    },
    '4': {
      '6': '&#177;6 = &#177;10 <span class="rotate">&#177;</span> 5 &#177; 1',
      '7': '&#177;7 = &#177;10 <span class="rotate">&#177;</span> 5 &#177; 2',
      '8': '&#177;8 = &#177;10 <span class="rotate">&#177;</span> 5 &#177; 3',
      '9': '&#177;9 = &#177;10 <span class="rotate">&#177;</span> 5 &#177; 4',
    },
  },
};
