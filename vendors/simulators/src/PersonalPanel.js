import React from 'react';
import PropTypes from 'prop-types';
import { PersonalPanelProvider } from './PersonalPanelContext';

const PersonalPanel = ({
  iconmin,
  dictation,
  args,
  params,
  controls,
  request,
  room,
  onStart,
  onCheck,
  onIncrement,
  onStop,
  onBegin,
  onEnd,
  onChangeInputValue,
  children,
  setfocus,
}) => {
  const fn = () => [];

  return (
    <PersonalPanelProvider
      iconmin={iconmin}
      setfocus={setfocus}
      dictation={dictation}
      args={args || []}
      params={params || {}}
      controls={controls || {}}
      request={request || {}}
      room={room || 'roomA'}
      onCheck={onCheck || fn}
      onIncrement={onIncrement || fn}
      onStart={onStart || fn}
      onBegin={onBegin || fn}
      onEnd={onEnd || fn}
      onStop={onStop || fn}
      onChangeInputValue={onChangeInputValue || null}
    >
      {children}
    </PersonalPanelProvider>
  );
};

PersonalPanel.defaultProps = {
  iconmin: false,
  dictation: false,
  request: {},
  args: [],
  setfocus: true,
  onChangeInputValue: null,
};

PersonalPanel.propTypes = {
  args: PropTypes.array,
  params: PropTypes.object.isRequired,
  controls: PropTypes.object.isRequired,
  request: PropTypes.object,
  room: PropTypes.string.isRequired,
  onBegin: PropTypes.func.isRequired,
  onEnd: PropTypes.func.isRequired,
  onStart: PropTypes.func.isRequired,
  onStop: PropTypes.func.isRequired,
  onCheck: PropTypes.func.isRequired,
  onIncrement: PropTypes.func.isRequired,
  onChangeInputValue: PropTypes.any,
  children: PropTypes.any.isRequired,
  iconmin: PropTypes.bool,
  setfocus: PropTypes.bool,
  dictation: PropTypes.bool,
};

export default PersonalPanel;
