import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Abacus from './menar/Abacus';

const abacusInitState = {
  a: [0, 0, 0, 0, 0],
  b: [0, 0, 0, 0, 0],
  c: [0, 0, 0, 0, 0],
  d: [0, 0, 0, 0, 0],
  e: [0, 0, 0, 0, 0],
};

const AbacusContainer = ({ state, handleAbacusChange }) => {
  const [abacusState, setAbacusState] = useState(abacusInitState);

  useEffect(() => {
    setAbacusState(state);
  }, [state]);

  const handleChange = (rodData, rodLable, nextAbacusState) => {
    handleAbacusChange(rodData, rodLable, nextAbacusState);
  };

  return <Abacus abacus={abacusState} handleChange={handleChange} />;
};

AbacusContainer.propTypes = {
  state: PropTypes.object.isRequired,
  handleAbacusChange: PropTypes.func.isRequired,
};

export default AbacusContainer;
