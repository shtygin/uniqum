import React, { useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import LinearProgress from '@material-ui/core/LinearProgress';

import { timer } from 'rxjs';
import { switchMap, filter, startWith, takeWhile } from 'rxjs/operators';

import { useEventCallback } from 'rxjs-hooks';
import { PersonalPanelContext } from './PersonalPanelContext';

const LinearProgressCustom = ({ period }) => {
  const [state] = useContext(PersonalPanelContext);
  const {
    workout: { end, begin },
  } = state;

  const [onEvent, frame] = useEventCallback(events$ =>
    events$.pipe(
      filter(e => e.type === 'progress_start'),
      switchMap(action =>
        timer(0, action.payload.period * 9).pipe(
          startWith(0),
          takeWhile(count => count < 100)
        )
      )
    )
  );

  useEffect(() => {
    if (end) {
      onEvent({
        type: 'progress_start',
        payload: { period },
      });
    }
  }, [end, begin]);

  return (
    <Grid item xs={12} lg={12} sm={12} md={12}>
      {end ? (
        <LinearProgress variant="determinate" value={frame} />
      ) : (
        <div style={{ height: '4px' }} />
      )}
    </Grid>
  );
};

LinearProgressCustom.propTypes = {
  period: PropTypes.number.isRequired,
};

export default LinearProgressCustom;
