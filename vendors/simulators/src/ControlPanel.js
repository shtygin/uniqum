import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import usePersonalControl from './usePersonalControl';

const ControlPanel = ({ visible }) => {
  const { state, patchControls } = usePersonalControl();
  const {
    workout,
    iconmin,
    setfocus,
    onChangeInputValue,
    controls: { value },
  } = state;

  const inputEl = useRef(null);

  const handleChange = event => {
    const { value: nextValue } = event.target;

    if (onChangeInputValue) {
      onChangeInputValue(nextValue);
    } else {
      patchControls('value', nextValue);
    }
  };

  const handleClick = () => {
    state.onCheck();
  };

  useEffect(() => {
    if (workout.end && setfocus) {
      inputEl.current.focus();
    }
  }, [workout.end]);

  const size = visible && visible.check ? 6 : 12;

  return (
    <Grid
      container
      item
      xs={12}
      lg={12}
      sm={12}
      md={12}
      style={{ padding: '10px' }}
    >
      <Grid item xs={size} lg={size} sm={size} md={size}>
        <TextField
          type="number"
          autoComplete="off"
          onChange={handleChange}
          value={value}
          variant="outlined"
          label="Введите ответ"
          inputRef={inputEl}
          InputProps={{ inputProps: { tabIndex: 1 } }}
        />
      </Grid>
      {visible.check && (
        <Grid item xs={6} lg={6} sm={6} md={6}>
          <Button
            variant="contained"
            color="primary"
            disabled={!workout.end}
            onClick={handleClick}
            startIcon={<FontAwesomeIcon icon={faCheck} />}
          >
            {!iconmin && 'Проверить'}
          </Button>
        </Grid>
      )}
    </Grid>
  );
};

ControlPanel.propTypes = {
  visible: PropTypes.object,
};

ControlPanel.defaultProps = {
  visible: {
    check: true,
  },
};

export default ControlPanel;
// https://upmostly.com/tutorials/how-to-use-the-usecontext-hook-in-react
