import React, { useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
// import Grid from '@material-ui/core/Grid';
import { tap, delay, filter } from 'rxjs/operators';
import { useEventCallback } from 'rxjs-hooks';
import { PersonalPanelContext } from './PersonalPanelContext';
import usePersonalControl from './usePersonalControl';

function timerEpic(events$) {
  return events$.pipe(
    filter(e => e.type === 'timer'),
    tap(action => action.deps.begin()),
    delay(100),
    tap(action => action.deps.end())
  );
}

const Column = ({ render }) => {
  const [state] = useContext(PersonalPanelContext);
  const {
    controls: { start },
    args,
  } = state;

  const { begin, end } = usePersonalControl();

  // const sum = args.reduce((acc, next) => acc + next, 0);
  // const size = 30 / args.length;
  // const sizeAnswer = Math.round(30 / String(sum).length);
  // let color = 'green';
  // if (Number(value) !== sum) {
  //   color = 'red';
  // }

  const [onEvent] = useEventCallback(timerEpic);

  useEffect(() => {
    if (start && args.length > 0) {
      onEvent({
        type: 'timer',
        deps: { begin, end },
      });
    }
  }, [args.length, start]);

  // const i = 0;

  return <>{render({ state })}</>;
};

Column.propTypes = {
  render: PropTypes.func.isRequired,
};

export default Column;

// <Grid item xs={12} lg={12} sm={12} md={12} style={{ minHeight: `25vh` }}>
//   {check ? (
//     <div style={{ color, fontSize: `${sizeAnswer}vh` }}>{sum}</div>
//   ) : (
//     <>
//       {args.map(num => (
//         <div key={(i += 1)} style={{ fontSize: `${size}vh` }}>
//           {request.success && num}
//         </div>
//       ))}
//     </>
//   )}
// </Grid>
