const speak = (audio_url, playing, ended) => {
  const audio = new Audio(audio_url);

  audio.autoplay = true;
  audio.preload = 'auto';

  audio.addEventListener(
    'playing',
    () => {
      playing();
    },
    false
  );

  audio.addEventListener(
    'ended',
    () => {
      ended();
    },
    false
  );

  audio.play();
};

export default speak;
