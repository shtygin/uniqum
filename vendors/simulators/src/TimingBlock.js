import React, { useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';

import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';

import usePersonalControl from './usePersonalControl';

const TimingBlock = () => {
  const {
    state: { getCount, count },
    change,
  } = usePersonalControl();
  const [minutes, setMinutes] = useState(5);

  const handleChange = event => {
    const coefficient = 1;
    const nextMinutes = event.target.value;
    setMinutes(nextMinutes);
    change('count', Math.round(nextMinutes * coefficient));
    getCount(count);
  };

  useEffect(() => {
    getCount(count);
  }, [count, getCount]);

  return (
    <Grid
      container
      item
      xs={12}
      lg={12}
      sm={12}
      md={12}
      style={{ padding: '10px' }}
    >
      <Grid
        item
        xs={12}
        lg={12}
        sm={12}
        md={12}
        style={{ position: 'relative' }}
      >
        <h3 style={{ margin: 0 }}>ПРИВЕТ ДРУГ!</h3>
        <h5 style={{ margin: 0 }}>Выбери сколько минту ты хочешь играть?</h5>
      </Grid>
      <Grid
        item
        xs={12}
        lg={12}
        sm={12}
        md={12}
        style={{ position: 'relative' }}
      >
        <FormControl>
          <InputLabel>Минуты</InputLabel>
          <Select
            value={minutes}
            onChange={handleChange}
            inputProps={{
              name: 'minutes',
            }}
          >
            <MenuItem value={5}>5</MenuItem>
            <MenuItem value={10}>10</MenuItem>
            <MenuItem value={15}>15</MenuItem>
            <MenuItem value={20}>20</MenuItem>
            <MenuItem value={25}>25</MenuItem>
            <MenuItem value={30}>30</MenuItem>
            <MenuItem value={40}>40</MenuItem>
            <MenuItem value={50}>50</MenuItem>
            <MenuItem value={60}>60</MenuItem>
          </Select>
        </FormControl>
      </Grid>
      <Grid
        item
        xs={12}
        lg={12}
        sm={12}
        md={12}
        style={{ position: 'relative' }}
      >
        <h5 style={{ margin: 0, marginBottom: '30px' }}>
          Тогда тебе сперва нужно решить{' '}
          <span style={{ fontSize: '25px', padding: '8px' }}>{count}</span>{' '}
          примеров, НЕВЕРНЫЕ ответы ни в счет!
        </h5>
      </Grid>
    </Grid>
  );
};

export default TimingBlock;
