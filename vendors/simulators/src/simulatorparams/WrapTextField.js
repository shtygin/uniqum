import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import Tooltip from '@material-ui/core/Tooltip';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import IconButton from '@material-ui/core/IconButton';

const labels = {
  range: 'Разряд',
  steps: 'Кол-во чисел',
  examples: 'Кол-во примеров',
  interval: 'Временной период, сек.',
  type: 'Тип тренажера',
  min: 'Диапазон, мин',
  max: 'Диапазон, макс',
};
const descriptions = {
  interval: `<p><em>Временной интервал задает скорость отображения слагаемых.</em></p>`,
  steps: `<p><em>В случае если равно 2:</em></p><ul><li>X + X</li></ul>
    <p><em>В случае если равно 3:</em></p><ul><li>X + X + X</li></ul>
    <p><em>В случае если равно 4:</em></p><ul><li>X + X + X + X</li></ul>
    <p><em>и так далее ...</em></p>
    <p><em>, где X - случайное число</em></p>`,
  range: `<p><em>В случае если равно 1:</em></p><ul><li>слагаемое будет 1 до 9</li></ul>
    <p><em>В случае если равно 2:</em></p><ul><li>слагаемое будет 1 до 99</li></ul>
    <p><em>В случае если равно 3:</em></p><ul><li>слагаемое будет 1 до 999</li></ul>
    <p><em>и так далее ...</em></p>`,
  min: `<p><em>Наименьшая граница числа</em></p>`,
  max: `<p><em>Наибольшая граница числа</em></p>`,
};

const DESC = ({ text }) => <div dangerouslySetInnerHTML={{ __html: text }} />;
DESC.propTypes = {
  text: PropTypes.string.isRequired,
};

const useStyles = makeStyles(() => ({
  textField: {
    margin: 8 * 2,
  },
  input: {
    marginTop: '5px',
  },
  formControl: {
    width: '100%',
    margin: 8 * 2,
  },
  inputLabel: {
    whiteSpace: 'nowrap',
  },
}));

export default function WrapTextField({
  prop,
  params,
  onValueChange,
  row,
  disabled,
  label,
}) {
  const { xs, sm, md, lg } = row;
  const classes = useStyles();
  return (
    <Grid item xs={xs} sm={sm} md={md} lg={lg}>
      <FormControl className={classes.formControl}>
        {label && (
          <InputLabel className={classes.inputLabel}>
            <Tooltip title={<DESC text={descriptions[prop]} />}>
              <IconButton aria-label="Активировать">
                <FontAwesomeIcon icon={faQuestionCircle} size="xs" />
              </IconButton>
            </Tooltip>
            {labels[prop]}
          </InputLabel>
        )}
        <Input
          className={classes.textField}
          classes={{ input: classes.input }}
          inputProps={{
            min: prop === 'interval' ? 0.1 : 1,
            step: prop === 'interval' ? 0.1 : 1,
            max: prop === 'range' ? 4 : 9999,
          }}
          onChange={onValueChange}
          name={prop}
          id={prop}
          disabled={disabled}
          value={params[prop]}
          type="number"
        />
      </FormControl>
    </Grid>
  );
}

WrapTextField.propTypes = {
  onValueChange: PropTypes.func.isRequired,
  params: PropTypes.object.isRequired,
  prop: PropTypes.string.isRequired,
  row: PropTypes.object.isRequired,
  disabled: PropTypes.bool,
  label: PropTypes.bool,
};

WrapTextField.defaultProps = {
  disabled: false,
  label: false,
};
