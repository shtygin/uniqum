import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import {
  xs,
  sm,
  md,
  lg,
  coefficient,
  NO_MODE,
  RULE_SIMPLE_SUM,
  RULE_FIVE_SUM,
  RULE_TEN_SUM,
  RULE_FIVE_AND_TEN_SUM,
} from '../utils/const';

const useStyles = makeStyles(() => ({
  select: {
    marginTop: '5px',
  },
  formControlForSelect: {
    margin: 8 * 2,
  },
  inputLabel: {
    whiteSpace: 'nowrap',
  },
  root: {
    paddingRight: '5px',
    paddingTop: '2px',
    paddingLeft: '0px',
    paddingBottom: '0px',
  },
}));

export default function ModeFiled({ params, onChangeParams, fullSize }) {
  const classes = useStyles();

  const handleChange = event => {
    const newParams = { ...params };
    const { name, value } = event.target;

    if (Number(value) === 2) {
      const diff = [1, 2, 3, 4].filter(x =>
        newParams.numbers.map(n => Number(n)).includes(x)
      );
      if (diff.length === 0) {
        newParams.numbers = [1, 2, 3, 4];
      } else {
        newParams.numbers = diff;
      }
    }
    if (Number(value) === 4) {
      const diff = [6, 7, 8, 9].filter(x =>
        newParams.numbers.map(n => Number(n)).includes(x)
      );
      if (diff.length === 0) {
        newParams.numbers = [6, 7, 8, 9];
      } else {
        newParams.numbers = diff;
      }
    }
    onChangeParams({ ...newParams, [name]: value });
  };

  return (
    <Grid
      item
      xs={fullSize ? 12 : Math.ceil(xs * coefficient.mode.xs)}
      sm={fullSize ? 12 : Math.ceil(sm * coefficient.mode.sm)}
      md={fullSize ? 12 : Math.ceil(md * coefficient.mode.md)}
      lg={fullSize ? 12 : Math.ceil(lg * coefficient.mode.lg)}
      key={-8}
    >
      <FormControl className={classes.formControlForSelect}>
        <InputLabel>
          <Tooltip
            className={classes.root}
            placement="right"
            title={
              <>
                <p>
                  <em>Прямой: без формул</em>
                </p>
                <p>
                  <em>Младшие товарищи (правила на 5 ):</em>
                </p>
                <ul>
                  <li>1 = 5 - 4</li>
                  <li>2 = 5 - 3</li>
                  <li>3 = 5 - 2</li>
                  <li>4 = 5 - 1</li>
                </ul>
                <p>
                  <em>Старшие товарищи (правила на 10 ):</em>
                </p>
                <ul>
                  <li>1 = 10 - 9</li>
                  <li>2 = 10 - 8</li>
                  <li>3 = 10 - 7</li>
                  <li>4 = 10 - 6</li>
                  <li>5 = 10 - 5</li>
                  <li>6 = 10 - 4</li>
                  <li>7 = 10 - 3</li>
                  <li>8 = 10 - 2</li>
                  <li>9 = 10 - 1</li>
                </ul>
                <p>
                  <em>Mix (правила на 5 и на 10 ):</em>
                </p>
                <ul>
                  <li>6 = 1 - 5 + 10</li>
                  <li>7 = 2 - 5 + 10</li>
                  <li>8 = 3 - 5 + 10</li>
                  <li>9 = 4 - 5 + 10</li>
                </ul>
                <p>
                  <em>Произвольно (формулы на любое правило ).</em>
                </p>
              </>
            }
          >
            <IconButton aria-label="Активировать">
              <FontAwesomeIcon icon={faQuestionCircle} size="xs" />
            </IconButton>
          </Tooltip>
          Правило
        </InputLabel>
        <Select
          value={params.mode}
          inputProps={{
            name: 'mode',
          }}
          classes={{ root: classes.select }}
          onChange={handleChange}
        >
          <MenuItem value={RULE_SIMPLE_SUM}>Без формул</MenuItem>
          <MenuItem value={RULE_FIVE_SUM}>Формулы на 5</MenuItem>
          <MenuItem value={RULE_TEN_SUM}>Формулы на 10</MenuItem>
          <MenuItem value={RULE_FIVE_AND_TEN_SUM}>Формулы на 5 и 10</MenuItem>
          <MenuItem value={NO_MODE}>Произвольно</MenuItem>
        </Select>
      </FormControl>
    </Grid>
  );
}

ModeFiled.propTypes = {
  onChangeParams: PropTypes.func.isRequired,
  params: PropTypes.object.isRequired,
  fullSize: PropTypes.bool.isRequired,
};
